﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gridtest
{
    class LogObject
    {
        public LogObject(string invID, DateTime dateSend, DateTime invDate, string invSeries, string invNumber, string status, string sign)
        {
            _id = ++lastID;
            InvID = invID;
            DateSend = dateSend;
            InvDate = invDate;
            InvSeries = invSeries;
            InvNumber = invNumber;
            Status = status;
            Sign = sign;
        }
        private static int lastID = 0;
        public int _id;
        public int ID { get { return _id; } }
        public string _invID;
        public string InvID { get { return _invID; } set { _invID = value; } }
        public DateTime _dateSend;
        public DateTime DateSend { get {return _dateSend;} set { _dateSend = value;}}
        public DateTime _invDate;
        public DateTime InvDate { get {return _invDate;} set { _invDate = value;}}
        public string _invSeries;
        public string InvSeries { get { return _invSeries; } set { _invSeries = value; } }
        public string _invNumber;
        public string InvNumber { get { return _invNumber; } set { _invNumber = value; } }
        public string _status;
        public string Status { get { return _status; } set { _status = value; } }
        public string _sign;
        public string Sign { get { return _sign; } set { _sign = value; } }
    }
}
