﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReadWriteCsv;
using System.IO;

namespace gridtest
{
    public partial class Form1 : Form
    {
        BindingSource bs = new BindingSource(); 
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<LogObject> ll = new List<LogObject>();
            using (CsvFileReader reader = new CsvFileReader("log.csv"))
            {
                CsvRow csvrow = new CsvRow();
                while (reader.ReadRow(csvrow))
                {
                    ll.Add(new LogObject(csvrow[0], DateTime.Parse(csvrow[2]), DateTime.Parse(csvrow[1]), csvrow[3], csvrow[4], csvrow[5], csvrow[6]));

                }
            }
            ll.Reverse();
            bs.DataSource = typeof(LogObject);
            foreach (LogObject lo in ll)
            {
                bs.Add(lo);
            }
            dataGridView1.DataSource = bs;
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[2].HeaderText = "Ημ. Αποστολής";
            dataGridView1.Columns[3].HeaderText = "Ημ. Παραστατικού";
            dataGridView1.Columns[4].HeaderText = "Σειρά";
            dataGridView1.Columns[5].HeaderText = "Αρ. Παραστατικού";
            dataGridView1.Columns[6].HeaderText = "Κατάσταση";
            dataGridView1.Columns[7].HeaderText = "Υπογραφή";
            bs.Filter = "Sign = '2'";
        }
    }
}
