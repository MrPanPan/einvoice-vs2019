﻿using DreamSolutions.EInvoice;
using log4net;
using log4net.Config;
using log4net.Core;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Resources;
using System.Windows.Forms;


namespace DreamSolutions.EInvoiceLog
{
    public partial class EInvoiceLogForm : Form
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EInvoiceLog.EInvoiceLogForm));
        private static ResourceManager LocRM = new ResourceManager("DreamSolutions.EInvoiceLog.EinvoiceLogStrings", typeof(EInvoiceLog.EInvoiceLogForm).Assembly);

        private DataSet ds = new DataSet();
        private BindingSource bs = new BindingSource();
        private DataGridView dgv = new DataGridView();
        private DataGridViewTextBoxColumn printerDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn invIDDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn dateSendDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn invDateDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn invSeriesDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn invNumberDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        //private DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewLinkColumn signDataGridViewTextBoxColumn = new DataGridViewLinkColumn();
        private DataGridViewImageColumn updatedColumn = new DataGridViewImageColumn();

        private UltraDateTimePicker dateTimePicker3 = new UltraDateTimePicker();
        private UltraDateTimePicker dateTimePicker1 = new UltraDateTimePicker();

        private int limit = 25;
        private int offset = 0;
        private int count = 0;
        private string where = "";
        private string order = "";

        private string dt1;
        private string dt2;

        private List<string> printers = new List<string>();
        private string printer;

        private Icon notupdatedIcon;
        private Icon updatedIcon;

        DbConnection sql_con;
        EInvoiceDB eiDB;
        ProgressForm progressForm;
        float processedRows = 0.0F;

        public EInvoiceLogForm()
        {
            InitializeComponent();
            // GridView
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AutoGenerateColumns = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.printerDataGridViewTextBoxColumn,
            this.invIDDataGridViewTextBoxColumn,
            this.dateSendDataGridViewTextBoxColumn,
            this.invDateDataGridViewTextBoxColumn,
            this.invSeriesDataGridViewTextBoxColumn,
            this.invNumberDataGridViewTextBoxColumn,
            //this.statusDataGridViewTextBoxColumn,
            this.signDataGridViewTextBoxColumn,
            this.updatedColumn});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.dgv.Location = new System.Drawing.Point(0, 150);
            this.dgv.Name = "dgv";
            this.dgv.CellFormatting += dgv_CellFormatting;
            this.dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.ContextMenuStrip = this.contextMenuStrip1;
            this.dgv.DataSource = bs;
            this.dgv.ReadOnly = true;
            //this.dgv.Size = new System.Drawing.Size(877, 380);
            //this.dgv.TabIndex = 100;
            this.dgv.TabStop = false;

            // Dataset
            this.ds.DataSetName = "DataSet1";
            //this.ds.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;

            // BindingSource
            //this.bs.DataMember = "LogTable";
            //this.bs.DataSource = this.ds.Tables["LogTable"].DefaultView;
            //this.bs.Filter = "dateSend >= '2015-09-27T15:30:00'";
            // 
            // printerDataGridViewTextBoxColumn
            // 
            this.printerDataGridViewTextBoxColumn.DataPropertyName = "printer";
            this.printerDataGridViewTextBoxColumn.HeaderText = LocRM.GetString("dgvPrinter");
            this.printerDataGridViewTextBoxColumn.Name = "printerDataGridViewTextBoxColumn";
            this.printerDataGridViewTextBoxColumn.ReadOnly = true;
            this.printerDataGridViewTextBoxColumn.Visible = false;
            // 
            // invIDDataGridViewTextBoxColumn
            // 
            this.invIDDataGridViewTextBoxColumn.DataPropertyName = "invID";
            this.invIDDataGridViewTextBoxColumn.HeaderText = LocRM.GetString("dgvInvoiceID");
            this.invIDDataGridViewTextBoxColumn.Name = "invIDDataGridViewTextBoxColumn";
            this.invIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.invIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // dateSendDataGridViewTextBoxColumn
            // 
            this.dateSendDataGridViewTextBoxColumn.DataPropertyName = "dateSend";
            this.dateSendDataGridViewTextBoxColumn.HeaderText = LocRM.GetString("dgvDateSend");
            this.dateSendDataGridViewTextBoxColumn.Name = "dateSendDataGridViewTextBoxColumn";
            this.dateSendDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // invDateDataGridViewTextBoxColumn
            // 
            this.invDateDataGridViewTextBoxColumn.DataPropertyName = "invDate";
            this.invDateDataGridViewTextBoxColumn.HeaderText = LocRM.GetString("dgvInvoiceDate");
            this.invDateDataGridViewTextBoxColumn.Name = "invDateDataGridViewTextBoxColumn";
            this.invDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // invSeriesDataGridViewTextBoxColumn
            // 
            this.invSeriesDataGridViewTextBoxColumn.DataPropertyName = "invSeries";
            this.invSeriesDataGridViewTextBoxColumn.HeaderText = LocRM.GetString("dgvSeries");
            this.invSeriesDataGridViewTextBoxColumn.Name = "invSeriesDataGridViewTextBoxColumn";
            this.invSeriesDataGridViewTextBoxColumn.ReadOnly = true;

            // 
            // invNumberDataGridViewTextBoxColumn
            // 
            this.invNumberDataGridViewTextBoxColumn.DataPropertyName = "invNumber";
            this.invNumberDataGridViewTextBoxColumn.HeaderText = LocRM.GetString("dgvInvoiceNumber");
            this.invNumberDataGridViewTextBoxColumn.Name = "invNumberDataGridViewTextBoxColumn";
            this.invNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.invNumberDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            //this.statusDataGridViewTextBoxColumn.DataPropertyName = "status";
            //this.statusDataGridViewTextBoxColumn.HeaderText = "status";
            //this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            //this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            //this.statusDataGridViewTextBoxColumn.Visible = false;
            // 
            // signDataGridViewTextBoxColumn
            // 
            this.signDataGridViewTextBoxColumn.DataPropertyName = "sign";
            this.signDataGridViewTextBoxColumn.HeaderText = LocRM.GetString("dgvSign");
            this.signDataGridViewTextBoxColumn.Name = "signDataGridViewTextBoxColumn";
            this.signDataGridViewTextBoxColumn.ReadOnly = true;
            this.signDataGridViewTextBoxColumn.ValueType = typeof(string);
            this.signDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.signDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // updatedColumn
            // 
            this.updatedColumn.DataPropertyName = "updated";
            this.updatedColumn.HeaderText = LocRM.GetString("dgvUpdated");
            this.updatedColumn.Name = "updatedColumn";
            this.updatedColumn.ReadOnly = true;
            this.updatedColumn.ValuesAreIcons = true;
            this.updatedColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.updatedColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //
            // Add dgv to panel2
            //
            this.panel2.SuspendLayout();
            this.panel2.Controls.Add(dgv);
            this.panel2.ResumeLayout(true);
            //
            // DTPs and add to panel1
            //
            this.panel1.SuspendLayout();
            //
            //  Invoice date picker
            //
            this.dateTimePicker3.BeginInit();
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(474, 67);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker3.TabIndex = 30;
            this.dateTimePicker3.Value = null;
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            this.panel1.Controls.Add(this.dateTimePicker3);
            this.dateTimePicker3.EndInit();
            //
            // Send date picker
            //
            this.dateTimePicker1.BeginInit();
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(474, 40);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 25;
            this.dateTimePicker1.Value = null;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.dateTimePicker1.EndInit();
            // Created and added
            this.panel1.ResumeLayout(true);
            //
            // Progress form
            //
            this.progressForm = new ProgressForm();
            this.progressForm.Canceled += new EventHandler<EventArgs>(buttonCancel_Click);
            this.progressForm.Done += new EventHandler<EventArgs>(buttonDone_Click);
            this.progressForm.FormClosing += progressForm_FormClosing;

            this.PerformLayout();
            this.pagesCombo.SelectedIndex = 1;
            this.pagesCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.updatedIcon = new Icon(this.GetType(), "updated.ico");
            this.notupdatedIcon = new Icon(this.GetType(), "notupdated.ico");

            string logConfFile = Constants.InstallDir + @"\logConfig.xml";
            if (File.Exists(logConfFile))
            {
                XmlConfigurator.Configure(new System.IO.FileInfo(logConfFile));
            }
            else
            {
                Console.WriteLine("Missing log configuration");
                System.Environment.Exit(1);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                GetPrinters();
            } catch (SystemException ex)
            {
                log.Error("eINVOICE Log: " + ex.Message);
                MessageBox.Show(LocRM.GetString("noPrinters"), LocRM.GetString("errorHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            try
            {
                this.eiDB = new EInvoiceDB();
                sql_con = this.eiDB.GetConnection();
            }
            catch (SystemException ex)
            {
                log.Error("eINVOICE Log: " + ex.Message);
                MessageBox.Show(ex.Message);
            }
            this.RefreshData();
        }
        void RefreshData()
        {
            GetCount();
            order = "ORDER BY dateSend DESC";
            GetData();
            GetNav();
            this.dgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
        }
        private void EInvoiceLogForm_Shown(object sender, EventArgs e)
        {
            this.dgv.ClearSelection();
        }

        void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == this.dgv.Columns["updatedColumn"].Index)
            {
                if (e.Value.ToString() == "1") e.Value = this.updatedIcon;
                else e.Value = this.notupdatedIcon;
            }
        }

        private void GetPrinters()
        {
#if DEBUG
            printers.Add("DEBUGPRINTER");
            printers.Add("DEBUGPRINTER2");
#else
            RegistryKey eiPrintersKey = Registry.LocalMachine.OpenSubKey(Constants.REGKEY);
            if (eiPrintersKey == null)
            {
                throw new SystemException("Non existent eINVOICE printers key in registry");
            }
            foreach (string pr in eiPrintersKey.GetSubKeyNames())
            {
                printers.Add(pr);
            }
#endif
            this.printersCombo.Items.AddRange(printers.ToArray());
            this.printersCombo.SelectedIndex = 0;
            printer = printers[0];
            this.printersCombo.SelectedIndexChanged += comboBox2_SelectedIndexChanged;
        }

        private void GetData()
        {
            sql_con.Open();
            DbCommand cmd = null;
            try { 
                cmd = this.eiDB.factory.CreateCommand();
                if (this.eiDB.DBProvider.Contains("SQLite"))
                    cmd.CommandText = String.Format("SELECT printer,invID,dateSend,invDate,invSeries,invNumber,sign,updated from einvoicelog where printer='{4}' {0} {3} limit {1},{2}", where, offset, limit, order, printer);
                else if (this.eiDB.DBProvider.Contains("SqlClient"))
                    cmd.CommandText = String.Format("select top {2} * from ( SELECT printer,invID,dateSend,invDate,invSeries,invNumber,sign,updated, row_number() over ({3}) as r_n_n from einvoicelog where printer='{4}' {0}) xx where r_n_n >= {1}", where, offset, limit, order, printer);
                else
                    throw new NotImplementedException("Provider not implemented");
                cmd.Connection = sql_con;
                DbDataAdapter DB = this.eiDB.factory.CreateDataAdapter();
                DB.SelectCommand = cmd;
                ds.Reset();
                DB.Fill(ds);
                this.bs.DataSource = this.ds.Tables[0].DefaultView;
            }
            catch (SystemException ex)
            {
                log.Error("eINVOICE Log: " + ex.Message);
                MessageBox.Show("eINVOICE Log: " + ex.Message, LocRM.GetString("errorHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Dispose();
                sql_con.Close();
            }
        }
        private void GetCount()
        {
            
            sql_con.Open();
            DbCommand cmd = null;
            try
            {
                cmd = this.eiDB.factory.CreateCommand();
                cmd.CommandText = string.Format("select count(*) as count from einvoicelog where printer='{0}' ", printer) + where;
                cmd.Connection = sql_con;
                DbDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    this.recsTotalLabel.Text = reader["count"].ToString();
                    count = Convert.ToInt32(reader["count"].ToString());
                }
            }
            catch (SystemException ex)
            {
                log.Error("eINVOICE Log: " + ex.Message);
                MessageBox.Show("eINVOICE Log: " + ex.Message, LocRM.GetString("errorHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Dispose();
                sql_con.Close();
            }
        }
        private void GetNav()
        {
            if (count > 0)
            {
                this.recsFromLabel.Text = String.Format("{0}", offset + 1);
            }
            else
            {
                this.recsFromLabel.Text = "0";
            }
            if(count > offset+limit)
                this.recsToLabel.Text = String.Format("{0}", offset + limit);
            else
                this.recsToLabel.Text = String.Format("{0}", count);
            if (count > offset + limit) this.nextpageButton.Enabled = true;
            else this.nextpageButton.Enabled = false;
            if (offset - limit >= 0) this.prevpageButton.Enabled = true;
            else this.prevpageButton.Enabled = false;
        }

        private void nextpageButton_Click(object sender, EventArgs e)
        {
            offset += limit;
            GetCount();
            GetData();
            GetNav();
        }

        private void prevpageButton_Click(object sender, EventArgs e)
        {
            offset -= limit;
            GetCount();
            GetData();
            GetNav();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            dt1 = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", this.dateTimePicker1.Value);
        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            dt2 = string.Format("{0:yyyy-MM-ddTHH:mm:ss}", this.dateTimePicker3.Value);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.dateTimePicker1.Value = null;
            this.dateTimePicker3.Value = null;
            this.invmuber.Text = "";
            order = "ORDER BY dateSend DESC";
            offset = 0;
            where = "";
            dt1 = null;
            dt2 = null;
            GetCount();
            GetData();
            GetNav();
            this.dgv.ClearSelection();
        }

        private void findButton_Click(object sender, EventArgs e)
        {
            List<string> args = new List<string>();
            string invn = this.invmuber.Text.Trim().Replace("'","").Replace("*", "%");
            if (invn != "")
            {
                args.Add(string.Format(" invNumber LIKE '{0}' ", invn));
            }
            if (dt1 != null)
            {
                args.Add(string.Format(" dateSend >= '{0}' ", dt1));
                order = "ORDER BY dateSend ASC";
            }
            if (dt2 != null)
            {
                args.Add(string.Format(" invDate >= '{0}' ", dt2));
                order = "ORDER BY invDate ASC";
            }
            if (args.Count > 0)
                where = " AND " + string.Join(" AND ", args.ToArray());

            GetCount();
            GetData();
            GetNav();
        }
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == Constants.SIGNCOLUMN && e.RowIndex >= 0)
            {
                string Sign;
                Sign = (string)this.dgv.Rows[e.RowIndex].Cells[Constants.SIGNCOLUMN].Value;
                //System.Diagnostics.Process.Start(Constants.EIURL + "v/" + Sign);
                System.Diagnostics.Process.Start(Constants.WebApiUrl + "v/" + Sign);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            limit = Convert.ToInt32(this.pagesCombo.SelectedItem);
            offset = 0;
            GetCount();
            GetData();
            GetNav();
        }

        void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            printer = (string)this.printersCombo.SelectedItem;
            offset = 0;
            GetCount();
            GetData();
            GetNav();
            this.dgv.ClearSelection();
        }

        #region Progress form & background worker
        //
        // Progress form & background worker
        //
        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                this.progressForm.buttonCancel.Visible = true;
                this.progressForm.buttonDone.Visible = false;
                this.progressForm.Message = LocRM.GetString("inProgress");
                this.progressForm.StartPosition = FormStartPosition.CenterParent;
                this.processedRows = 0.0F;
                this.backgroundWorker1.RunWorkerAsync();
                this.progressForm.ShowDialog(this);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (this.backgroundWorker1.IsBusy)
            {
                this.backgroundWorker1.CancelAsync();
                this.progressForm.Hide();
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            this.progressForm.Hide();

        }

        private void progressForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.backgroundWorker1.CancellationPending)
            {
                this.backgroundWorker1.CancelAsync();
            }
            e.Cancel = true;
            this.progressForm.Hide();
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            var rows = this.dgv.SelectedRows;
            int totalRows = rows.Count;
            BackgroundWorker worker = sender as BackgroundWorker;
            try
            {
                EInvoiceObject eio = new EInvoiceObject(printer);
                foreach (var row in rows)
                {
                    if (worker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        break;
                    }
                    else
                    {
                        if (((DataGridViewRow)row).Cells[Constants.SIGNCOLUMN + 1].Value.ToString() != "1")
                        {
                            eio.UpdateSign(((DataGridViewRow)row).Cells[1].Value.ToString(), ((DataGridViewRow)row).Cells[Constants.SIGNCOLUMN].Value.ToString());
                        }
                        processedRows += 1.0F;
                        worker.ReportProgress((int)((processedRows / totalRows) * 100));
#if DEBUG
                        System.Threading.Thread.Sleep(1000);
#endif
                    }
                }
            }
            catch (SystemException ex)
            {
                e.Cancel = true;
                MessageBox.Show(ex.Message, LocRM.GetString("errorHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            this.progressForm.Message = LocRM.GetString("inProgress") + e.ProgressPercentage.ToString() + "%";
            this.progressForm.ProgressValue = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            this.RefreshData();
            this.dgv.ClearSelection();
            if (!e.Cancelled)
            {
                this.progressForm.buttonCancel.Visible = false;
                this.progressForm.buttonDone.Visible = true;
                this.progressForm.Message = LocRM.GetString("updated") + processedRows.ToString() + LocRM.GetString("records");
            }
            else
            {
                this.progressForm.Hide();
            }
        }

        #endregion
    }
}
