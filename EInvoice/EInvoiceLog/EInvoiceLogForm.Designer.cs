﻿namespace DreamSolutions.EInvoiceLog
{
    partial class EInvoiceLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EInvoiceLogForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.filtersLabel = new System.Windows.Forms.Label();
            this.printersCombo = new System.Windows.Forms.ComboBox();
            this.printersLabel = new System.Windows.Forms.Label();
            this.pagesCombo = new System.Windows.Forms.ComboBox();
            this.recsPerPageLabel = new System.Windows.Forms.Label();
            this.findButton = new System.Windows.Forms.Button();
            this.invmuber = new System.Windows.Forms.TextBox();
            this.invNumberLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.invDateLabel = new System.Windows.Forms.Label();
            this.dateSendLabel = new System.Windows.Forms.Label();
            this.nextpageButton = new System.Windows.Forms.Button();
            this.prevpageButton = new System.Windows.Forms.Button();
            this.recsToLabel = new System.Windows.Forms.Label();
            this.untilLabel = new System.Windows.Forms.Label();
            this.recsFromLabel = new System.Windows.Forms.Label();
            this.displayedLabel = new System.Windows.Forms.Label();
            this.recsTotalLabel = new System.Windows.Forms.Label();
            this.recsLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.filtersLabel);
            this.panel1.Controls.Add(this.printersCombo);
            this.panel1.Controls.Add(this.printersLabel);
            this.panel1.Controls.Add(this.pagesCombo);
            this.panel1.Controls.Add(this.recsPerPageLabel);
            this.panel1.Controls.Add(this.findButton);
            this.panel1.Controls.Add(this.invmuber);
            this.panel1.Controls.Add(this.invNumberLabel);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.invDateLabel);
            this.panel1.Controls.Add(this.dateSendLabel);
            this.panel1.Controls.Add(this.nextpageButton);
            this.panel1.Controls.Add(this.prevpageButton);
            this.panel1.Controls.Add(this.recsToLabel);
            this.panel1.Controls.Add(this.untilLabel);
            this.panel1.Controls.Add(this.recsFromLabel);
            this.panel1.Controls.Add(this.displayedLabel);
            this.panel1.Controls.Add(this.recsTotalLabel);
            this.panel1.Controls.Add(this.recsLabel);
            this.panel1.Name = "panel1";
            // 
            // filtersLabel
            // 
            resources.ApplyResources(this.filtersLabel, "filtersLabel");
            this.filtersLabel.Name = "filtersLabel";
            // 
            // printersCombo
            // 
            this.printersCombo.FormattingEnabled = true;
            resources.ApplyResources(this.printersCombo, "printersCombo");
            this.printersCombo.Name = "printersCombo";
            // 
            // printersLabel
            // 
            resources.ApplyResources(this.printersLabel, "printersLabel");
            this.printersLabel.Name = "printersLabel";
            // 
            // pagesCombo
            // 
            this.pagesCombo.FormattingEnabled = true;
            this.pagesCombo.Items.AddRange(new object[] {
            resources.GetString("pagesCombo.Items"),
            resources.GetString("pagesCombo.Items1"),
            resources.GetString("pagesCombo.Items2"),
            resources.GetString("pagesCombo.Items3")});
            resources.ApplyResources(this.pagesCombo, "pagesCombo");
            this.pagesCombo.Name = "pagesCombo";
            // 
            // recsPerPageLabel
            // 
            resources.ApplyResources(this.recsPerPageLabel, "recsPerPageLabel");
            this.recsPerPageLabel.Name = "recsPerPageLabel";
            // 
            // findButton
            // 
            resources.ApplyResources(this.findButton, "findButton");
            this.findButton.Name = "findButton";
            this.findButton.UseVisualStyleBackColor = true;
            this.findButton.Click += new System.EventHandler(this.findButton_Click);
            // 
            // invmuber
            // 
            resources.ApplyResources(this.invmuber, "invmuber");
            this.invmuber.Name = "invmuber";
            // 
            // invNumberLabel
            // 
            resources.ApplyResources(this.invNumberLabel, "invNumberLabel");
            this.invNumberLabel.Name = "invNumberLabel";
            // 
            // cancelButton
            // 
            resources.ApplyResources(this.cancelButton, "cancelButton");
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // invDateLabel
            // 
            resources.ApplyResources(this.invDateLabel, "invDateLabel");
            this.invDateLabel.Name = "invDateLabel";
            // 
            // dateSendLabel
            // 
            resources.ApplyResources(this.dateSendLabel, "dateSendLabel");
            this.dateSendLabel.Name = "dateSendLabel";
            // 
            // nextpageButton
            // 
            resources.ApplyResources(this.nextpageButton, "nextpageButton");
            this.nextpageButton.Name = "nextpageButton";
            this.nextpageButton.UseVisualStyleBackColor = true;
            this.nextpageButton.Click += new System.EventHandler(this.nextpageButton_Click);
            // 
            // prevpageButton
            // 
            resources.ApplyResources(this.prevpageButton, "prevpageButton");
            this.prevpageButton.Name = "prevpageButton";
            this.prevpageButton.UseVisualStyleBackColor = true;
            this.prevpageButton.Click += new System.EventHandler(this.prevpageButton_Click);
            // 
            // recsToLabel
            // 
            resources.ApplyResources(this.recsToLabel, "recsToLabel");
            this.recsToLabel.Name = "recsToLabel";
            // 
            // untilLabel
            // 
            resources.ApplyResources(this.untilLabel, "untilLabel");
            this.untilLabel.Name = "untilLabel";
            // 
            // recsFromLabel
            // 
            resources.ApplyResources(this.recsFromLabel, "recsFromLabel");
            this.recsFromLabel.Name = "recsFromLabel";
            // 
            // displayedLabel
            // 
            resources.ApplyResources(this.displayedLabel, "displayedLabel");
            this.displayedLabel.Name = "displayedLabel";
            // 
            // recsTotalLabel
            // 
            resources.ApplyResources(this.recsTotalLabel, "recsTotalLabel");
            this.recsTotalLabel.Name = "recsTotalLabel";
            // 
            // recsLabel
            // 
            resources.ApplyResources(this.recsLabel, "recsLabel");
            this.recsLabel.Name = "recsLabel";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            resources.ApplyResources(this.updateToolStripMenuItem, "updateToolStripMenuItem");
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // EInvoiceLogForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "EInvoiceLogForm";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.EInvoiceLogForm_Shown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label recsToLabel;
        private System.Windows.Forms.Label untilLabel;
        private System.Windows.Forms.Label recsFromLabel;
        private System.Windows.Forms.Label displayedLabel;
        private System.Windows.Forms.Label recsTotalLabel;
        private System.Windows.Forms.Label recsLabel;
        private System.Windows.Forms.Button nextpageButton;
        private System.Windows.Forms.Button prevpageButton;
        private System.Windows.Forms.Label invDateLabel;
        private System.Windows.Forms.Label dateSendLabel;
        private System.Windows.Forms.Label invNumberLabel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox invmuber;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.ComboBox pagesCombo;
        private System.Windows.Forms.Label recsPerPageLabel;
        private System.Windows.Forms.ComboBox printersCombo;
        private System.Windows.Forms.Label printersLabel;
        private System.Windows.Forms.Label filtersLabel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

