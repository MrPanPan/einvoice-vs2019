﻿!include "MUI2.nsh"
;!include "WordFunc.nsh"
!include LogicLib.nsh
;!include "winmessages.nsh"
;!include nsDialogs.nsh
!include "FileFunc.nsh"
!include x64.nsh
;!include "GetWindowsVersion.nsh"
;!include "Explode.nsh"
!include "..\EInvoice\templateScripts\nsisVersion.nsh"

!define PROGDIR "..\release\any"

!define APPNAME "eINVOICEGlobalClient"
!define COMPANYNAME "DreamSolutions"
!define DESCRIPTION "eINVOICE Global Client"

!define INSTALLSIZE 5900
!define HELPURL "http://www.dream-solutions.gr" # "Support Information" link
!define UPDATEURL "http://www.dream-solutions.gr" # "Product Updates" link
!define ABOUTURL "http://www.dream-solutions.gr" # "Publisher" link

BrandingText "eINVOICE Global Client Setup"
Name "eINVOICE Global Client"

InstallDir "C:\Program Files\DreamSolutions\eINVOICEGlobalClient"
RequestExecutionLevel admin


OutFile "..\Installer\eINVOICEGlobalClient-${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.exe"

VIAddVersionKey  "ProductName" "eINVOICE Global Client"
VIAddVersionKey  "Comments" "SoftOne's eINVOICE Global Client"
VIAddVersionKey  "CompanyName" "Dream Solutions"
VIAddVersionKey  "LegalTrademarks" "eINVOICE Global Client"
VIAddVersionKey  "LegalCopyright" "© Dream Solutions"
VIAddVersionKey  "FileDescription" "eINVOICE Global Client Setup"
VIAddVersionKey  "ProductVersion" "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}"
VIAddVersionKey  "FileVersion" "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}"
VIProductVersion "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}.${VERSIONREVISION}"

!define MUI_ICON ".\resources\logo31_4nu_icon.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP ".\resources\einvoice_left.bmp"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP ".\resources\einvoice_header.bmp"
!define MUI_ABORTWARNING
Icon ".\resources\logo31_4nu_icon.ico"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "$(MUILicense)"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY

!insertmacro MUI_PAGE_INSTFILES
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_RUN $INSTDIR\EInvoicePrinters.exe
!define MUI_FINISHPAGE_RUN_TEXT $(DESC_FinishPage)
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "Greek"
!insertmacro MUI_LANGUAGE "English"

LicenseLangString MUILicense ${LANG_GREEK} ".\resources\License.txt"
LicenseLangString MUILicense ${LANG_ENGLISH} ".\resources\LicenseEN.txt"

function .onInit
  setShellVarContext all
  ${If} ${RunningX64}
    StrCpy $INSTDIR "$PROGRAMFILES64\${COMPANYNAME}\${APPNAME}"
  ${Else}
    StrCpy $INSTDIR "$PROGRAMFILES\${COMPANYNAME}\${APPNAME}"
  ${EndIf}
functionEnd

function un.onInit
	SetShellVarContext all
functionEnd

Section "eINVOICE Global Client" SecEInvoice
  ${If} ${RunningX64}
    ${DisableX64FSRedirection}
    SetRegView 64
  ${EndIf}
  ExecShell "" "net" "stop spooler" SW_HIDE
  Sleep 3000
  SetDetailsView show
  CreateDirectory "$INSTDIR\x86"
  CreateDirectory "$INSTDIR\x64"
  CreateDirectory "$INSTDIR\el"

  SetOutPath "$INSTDIR"
  
  File ".\resources\License.txt"
  File ".\resources\LicenseEN.txt"
  File ${PROGDIR}\eINVOICEWebApiClient.dll
  File ${PROGDIR}\EInvoice.exe
  File ${PROGDIR}\EInvoice.exe.config
  File ${PROGDIR}\EInvoice.ico
  File ${PROGDIR}\Newtonsoft.Json.dll
  File ${PROGDIR}\EInvoiceDataSet.dll
  File ${PROGDIR}\EInvoiceDataSet.dll.config
  File ${PROGDIR}\EInvoiceLog.exe
  File ${PROGDIR}\EInvoiceLog.exe.config
  File ${PROGDIR}\EInvoiceLog.ico
  File ${PROGDIR}\EInvoicePrinters.exe
  File ${PROGDIR}\EInvoicePrinters.exe.config
  File ${PROGDIR}\EInvoicePrinters.ico
  IfFileExists "${PROGDIR}\einvoicedb.sqlite" +2
    File ${PROGDIR}\einvoicedb.sqlite
  File ${PROGDIR}\System.Data.SQLite.dll
  File ${PROGDIR}\System.Data.SQLite.Linq.dll
  File ${PROGDIR}\System.Data.SQLite.xml
  File ${PROGDIR}\log4net.xml
  File ${PROGDIR}\log4net.dll
  File ${PROGDIR}\logConfig.xml
  File ${PROGDIR}\sqlservercreatetable.sql
  File ${PROGDIR}\EIUpdateSign.dll
  SetOutPath "$INSTDIR\x86"
  
  File ${PROGDIR}\redmon32.dll
  File ${PROGDIR}\redmon.chm
  File ${PROGDIR}\x86\SQLite.Interop.dll
  
  SetOutPath "$INSTDIR\x64"
  File ${PROGDIR}\redmon64.dll
  File ${PROGDIR}\redmon.chm
  File ${PROGDIR}\x64\SQLite.Interop.dll

  SetOutPath "$INSTDIR\el"
  File ${PROGDIR}\el\EInvoicePrinters.resources.dll
  File ${PROGDIR}\el\EInvoiceLog.resources.dll
  File ${PROGDIR}\el\EInvoice.resources.dll

  SetOutPath "$INSTDIR"
  
  ${If} ${RunningX64}
    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Print\Monitors\Redirected Port" "Driver" "$INSTDIR\x64\redmon64.dll"
  ${Else}
    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Print\Monitors\Redirected Port" "Driver" "$INSTDIR\x86\redmon32.dll"
  ${EndIf}

  WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Print\Monitors\Redirected Port\Ports" "" ""
  WriteRegStr HKLM "SOFTWARE\DreamSolutions\eINVOICEGlobalClient" "InstallDir" "$INSTDIR"
  WriteRegStr HKLM "SOFTWARE\DreamSolutions\eINVOICEGlobalClient" "WebApiUrl" "https://einvoiceapp.softonecloud.com/"
  WriteRegStr HKLM "SOFTWARE\DreamSolutions\eINVOICEGlobalClient" "Version" "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}"
  WriteRegStr HKLM "SOFTWARE\DreamSolutions\eINVOICEGlobalClient\Printers" "" ""
  ExecShell "" "net" "start spooler" SW_HIDE
  Sleep 3000
  WriteUninstaller "$INSTDIR\Uninstall.exe"
    
  ;Create shortcuts
  CreateDirectory "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client"
  CreateShortcut "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client\eINVOICEPrinters.lnk" "$INSTDIR\EInvoicePrinters.exe" "" "$INSTDIR\EInvoicePrinters.ico" 0
  CreateShortcut "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client\eINVOICELog.lnk" "$INSTDIR\EInvoiceLog.exe" "" "$INSTDIR\EInvoiceLog.ico" 0 
  CreateShortcut "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayName" "${DESCRIPTION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "UninstallString" "$\"$INSTDIR\Uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "InstallLocation" "$\"$INSTDIR$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayIcon" "$\"$INSTDIR\EInvoice.ico$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "Publisher" "${COMPANYNAME}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "HelpLink" "$\"${HELPURL}$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLUpdateInfo" "$\"${UPDATEURL}$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLInfoAbout" "$\"${ABOUTURL}$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayVersion" "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMajor" ${VERSIONMAJOR}
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMinor" ${VERSIONMINOR}
	# There is no option for modifying or repairing the install
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoRepair" 1
	# Set the INSTALLSIZE constant (!defined at the top of this script) so Add/Remove Programs can accurately report the size
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "EstimatedSize" ${INSTALLSIZE}
  
  AccessControl::GrantOnFile "$INSTDIR" "(S-1-5-32-545)" "FullAccess"

  AccessControl::GrantOnFile "$INSTDIR" "(S-1-5-32-544)" "FullAccess"

  ${If} ${RunningX64}
    ${EnableX64FSRedirection}
  ${EndIf}
SectionEnd



;-----------------------------
; Language strings
LangString DESC_SecEInvoice ${LANG_GREEK} "eINVOICE Global Client. Ηλεκτρονική τιμολόγηση και αρχειοθέτηση για όλες της εφαρμογές."
LangString DESC_FinishPage ${LANG_GREEK} "Διαχείρηση εκτυπωτών eINVOICE."
LangString DESC_SecEInvoice ${LANG_ENGLISH} "eINVOICE Global Client. Electroniv invoicing and archiving for all applications."
LangString DESC_FinishPage ${LANG_ENGLISH} "Administer eINVOICE printers."


  ;Assign language strings to sections
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SecEInvoice} $(DESC_SecEInvoice)
!insertmacro MUI_FUNCTION_DESCRIPTION_END


;--------------------------------
;Uninstaller Section

Section "Uninstall"
  ${If} ${RunningX64}
    ${DisableX64FSRedirection}
    SetRegView 64
  ${EndIf}
  ExecShell "" "net" "stop spooler" SW_HIDE
  Sleep 3000
  Delete "$INSTDIR\eINVOICEWebApiClient.dll"
  Delete "$INSTDIR\EInvoice.exe"
  Delete "$INSTDIR\EInvoice.exe.config"
  Delete "$INSTDIR\EInvoice.ico"
  Delete "$INSTDIR\Newtonsoft.Json.dll"
  Delete "$INSTDIR\EInvoiceDataSet.dll"
  Delete "$INSTDIR\EInvoiceDataSet.dll.config"
  Delete "$INSTDIR\EInvoiceLog.exe"
  Delete "$INSTDIR\EInvoiceLog.exe.config"
  Delete "$INSTDIR\EInvoiceLog.ico"
  Delete "$INSTDIR\EInvoicePrinters.exe"
  Delete "$INSTDIR\EInvoicePrinters.exe.config"
  Delete "$INSTDIR\EInvoicePrinters.ico"
  
  Delete "$INSTDIR\System.Data.SQLite.dll"
  Delete "$INSTDIR\System.Data.SQLite.Linq.dll"
  Delete "$INSTDIR\System.Data.SQLite.xml"
  Delete "$INSTDIR\log4net.xml"
  Delete "$INSTDIR\log4net.dll"
  Delete "$INSTDIR\logConfig.xml"
  Delete "$INSTDIR\sqlservercreatetable.sql"
  Delete "$INSTDIR\einvoice.log"
  Delete "$INSTDIR\EIUpdateSign.dll"
  Delete "$INSTDIR\License.txt"
  Delete "$INSTDIR\*.log"
  Delete "$INSTDIR\*.json"
  
  Delete "$INSTDIR\x86\redmon32.dll"
  Delete "$INSTDIR\x86\redmon.chm"
  Delete "$INSTDIR\x86\SQLite.Interop.dll"

  RMDIR "$INSTDIR\x86"

  Delete "$INSTDIR\x64\redmon64.dll"
  Delete "$INSTDIR\x64\redmon.chm"
  Delete "$INSTDIR\x64\SQLite.Interop.dll"

  RMDIR "$INSTDIR\x64"

  Delete "$INSTDIR\el\EInvoicePrinters.resources.dll"
  Delete "$INSTDIR\el\EInvoiceLog.resources.dll"
  Delete "$INSTDIR\el\EInvoice.resources.dll"

  RMDIR "$INSTDIR\el"

  Delete "$INSTDIR\Uninstall.exe"

  ;RMDir "$INSTDIR"
  
  Delete "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client\Uninstall.lnk"
  Delete "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client\eINVOICEPrinters.lnk"
  Delete "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client\eINVOICELog.lnk"
  RMDir "$SMPROGRAMS\DreamSolutions\eINVOICE Global Client"
  StrCpy $0 0
  loop:
    EnumRegKey $1 HKLM "Software\DreamSolutions\eINVOICEGlobalClient\Printers" $0
    StrCmp $1 "" done
    DeleteRegKey HKLM "Software\DreamSolutions\eINVOICEGlobalClient\Printers\$1"
    Goto loop
  done:
  DeleteRegKey HKLM "Software\DreamSolutions\eINVOICEGlobalClient\Printers"
  DeleteRegKey HKLM "Software\DreamSolutions\eINVOICEGlobalClient"  
  DeleteRegKey /ifempty HKLM "Software\DreamSolutions"
  DeleteRegKey HKLM "SYSTEM\CurrentControlSet\Control\Print\Monitors\Redirected Port"
  ExecShell "" "net" "start spooler" SW_HIDE
  Sleep 3000
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}"
  ${If} ${RunningX64}
    ${EnableX64FSRedirection}
  ${EndIf}
SectionEnd