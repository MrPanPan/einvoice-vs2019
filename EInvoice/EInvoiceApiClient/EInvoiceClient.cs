﻿using System.Text;
using System.Net;
using System.IO;
using eINVOICEWebApiClient;
using DreamSolutions.EInvoiceDataSet;

namespace DreamSolutions.EInvoiceApiClient
{
    public class EInvoiceClient
    {
        private string eiUrl;
        public EInvoiceClient(string url)
        {
            this.eiUrl = url;
        }
        public WebApiClient.SendDocumentResponse SendInvoice(string secret, string method, string info, EIDataSet eids)
        {
            WebApiClient wac = new WebApiClient(this.eiUrl, eids.getVersion());
            return wac.SendDocument(secret, method, info, eids.ToFormat(wac));
            /*string address = string.Format("{0}api/Invoice/?secretkey={1}&sendMethod={2}&clientInfo={3}", this.eiUrl, secret, method, info);
            WebClient wc = new WebClient();
            EIResponse resp = null;
            try
            {
                ServicePointManager.Expect100Continue = false;
                wc.Headers.Add("Content-Type", "text/plain");
                wc.Encoding = Encoding.UTF8;
                try
                {
                    string wr = wc.UploadString(address, "POST", formatted);
                    resp = new EIResponse(EIEnum.OK, "", wr.Replace("\"", ""));
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        HttpWebResponse wr = (HttpWebResponse)ex.Response;
                        string respRead = ((string)new StreamReader(wr.GetResponseStream()).ReadToEnd()).Replace("\"", "");
                        if (wr.StatusCode == HttpStatusCode.Created)
                        {
                            resp = new EIResponse(EIEnum.OK, "", respRead);
                        }
                        else if (wr.StatusCode == HttpStatusCode.Conflict)
                        {
                            resp = new EIResponse(EIEnum.ERROR, "Διπλότυπο παραστατικό:\r\n" + respRead, "");
                        }
                        else
                        {
                            resp = new EIResponse(EIEnum.ERROR, respRead, "");
                        }
                    }
                    else
                    {
                        resp = new EIResponse(EIEnum.ERROR, ex.Message, "");
                    }
                }
            }
            catch (System.ArgumentNullException ex)
            {
                resp = new EIResponse(EIEnum.ERROR, ex.Message, "");
            }
            catch (System.SystemException ex)
            {
                resp = new EIResponse(EIEnum.ERROR, ex.Message, "");
            }
            finally
            {
                if (wc != null) wc.Dispose();
            }
            return resp;*/
        }
        /*
        public class EIResponse
        {
            public EIEnum Status;
            public string Message;
            public string Sign;

            public EIResponse(EIEnum status, string message, string sign)
            {
                this.Status = status;
                this.Message = message;
                this.Sign = sign;
            }
        }

        public enum EIEnum
        {
            OK, WARNING, ERROR
        }
         * */
    }
}
