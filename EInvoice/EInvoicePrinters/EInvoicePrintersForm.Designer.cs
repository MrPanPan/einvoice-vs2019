﻿namespace DreamSolutions.EInvoicePrinters
{
    partial class EInvoicePrintersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EInvoicePrintersForm));
            this.printersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.printersDataSet = new DreamSolutions.EInvoicePrinters.PrintersDataSet();
            this.label3 = new System.Windows.Forms.Label();
            this.printerSecretTB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.printerNumbersCB = new System.Windows.Forms.CheckBox();
            this.printerEncodingCB = new System.Windows.Forms.ComboBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonAddPrinter = new System.Windows.Forms.Button();
            this.printerNamesCB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.printerDatesCB = new System.Windows.Forms.CheckBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonDeletePrinter = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.printerNoDebugCB = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.printersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printersDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // printersBindingSource
            // 
            this.printersBindingSource.DataMember = "Printers";
            this.printersBindingSource.DataSource = this.printersDataSet;
            // 
            // printersDataSet
            // 
            this.printersDataSet.DataSetName = "PrintersDataSet";
            this.printersDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // printerSecretTB
            // 
            resources.ApplyResources(this.printerSecretTB, "printerSecretTB");
            this.printerSecretTB.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.printersBindingSource, "Secret", true));
            this.printerSecretTB.Name = "printerSecretTB";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // printerNumbersCB
            // 
            resources.ApplyResources(this.printerNumbersCB, "printerNumbersCB");
            this.printerNumbersCB.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.printersBindingSource, "GrNumbers", true));
            this.printerNumbersCB.Name = "printerNumbersCB";
            this.printerNumbersCB.UseVisualStyleBackColor = true;
            // 
            // printerEncodingCB
            // 
            resources.ApplyResources(this.printerEncodingCB, "printerEncodingCB");
            this.printerEncodingCB.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.printersBindingSource, "Encoding", true));
            this.printerEncodingCB.FormattingEnabled = true;
            this.printerEncodingCB.Items.AddRange(new object[] {
            resources.GetString("printerEncodingCB.Items"),
            resources.GetString("printerEncodingCB.Items1")});
            this.printerEncodingCB.Name = "printerEncodingCB";
            // 
            // buttonSave
            // 
            resources.ApplyResources(this.buttonSave, "buttonSave");
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAddPrinter
            // 
            resources.ApplyResources(this.buttonAddPrinter, "buttonAddPrinter");
            this.buttonAddPrinter.Name = "buttonAddPrinter";
            this.buttonAddPrinter.UseVisualStyleBackColor = true;
            this.buttonAddPrinter.Click += new System.EventHandler(this.buttonAddPrinter_Click);
            // 
            // printerNamesCB
            // 
            resources.ApplyResources(this.printerNamesCB, "printerNamesCB");
            this.printerNamesCB.DataSource = this.printersBindingSource;
            this.printerNamesCB.DisplayMember = "Name";
            this.printerNamesCB.FormattingEnabled = true;
            this.printerNamesCB.Name = "printerNamesCB";
            this.printerNamesCB.ValueMember = "Name";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // printerDatesCB
            // 
            resources.ApplyResources(this.printerDatesCB, "printerDatesCB");
            this.printerDatesCB.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.printersBindingSource, "GrDates", true));
            this.printerDatesCB.Name = "printerDatesCB";
            this.printerDatesCB.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonDeletePrinter
            // 
            resources.ApplyResources(this.buttonDeletePrinter, "buttonDeletePrinter");
            this.buttonDeletePrinter.Name = "buttonDeletePrinter";
            this.buttonDeletePrinter.UseVisualStyleBackColor = true;
            this.buttonDeletePrinter.Click += new System.EventHandler(this.buttonDeletePrinter_Click);
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.buttonDeletePrinter);
            this.groupBox1.Controls.Add(this.buttonAddPrinter);
            this.groupBox1.Controls.Add(this.buttonCancel);
            this.groupBox1.Controls.Add(this.buttonSave);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // statusStrip1
            // 
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Name = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.printerNoDebugCB);
            this.groupBox2.Controls.Add(this.printerEncodingCB);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.printerSecretTB);
            this.groupBox2.Controls.Add(this.printerDatesCB);
            this.groupBox2.Controls.Add(this.printerNumbersCB);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // printerNoDebugCB
            // 
            resources.ApplyResources(this.printerNoDebugCB, "printerNoDebugCB");
            this.printerNoDebugCB.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.printersBindingSource, "NoDebug", true));
            this.printerNoDebugCB.Name = "printerNoDebugCB";
            this.printerNoDebugCB.UseVisualStyleBackColor = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // EInvoicePrintersForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.printerNamesCB);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "EInvoicePrintersForm";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.printersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printersDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox printerSecretTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox printerNumbersCB;
        private System.Windows.Forms.BindingSource printersBindingSource;
        private PrintersDataSet printersDataSet;
        private System.Windows.Forms.ComboBox printerEncodingCB;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonAddPrinter;
        private System.Windows.Forms.ComboBox printerNamesCB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox printerDatesCB;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonDeletePrinter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox printerNoDebugCB;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

