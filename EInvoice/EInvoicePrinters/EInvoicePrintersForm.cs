﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Resources;
using DreamSolutions.EInvoice;
using Microsoft.Win32;
using System.Diagnostics;
using System.Threading;
using log4net;
using log4net.Config;
using log4net.Core;
using System.IO;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace DreamSolutions.EInvoicePrinters
{
    public partial class EInvoicePrintersForm : Form
    {
        RegistryKey eiPrintersKey;
        bool changed;
        bool isReadOnly = false;
        ProgressForm progressForm;
        float processedRows = 0.0F;
        private static ResourceManager LocRM = new ResourceManager("DreamSolutions.EInvoicePrinters.EinvoicePrintersStrings", typeof(EInvoicePrinters.EInvoicePrintersForm).Assembly);
        private static readonly ILog log = LogManager.GetLogger(typeof(EInvoicePrinters.EInvoicePrintersForm));

        public EInvoicePrintersForm()
        {
            InitializeComponent();
            printerNumbersCB.DataBindings[0].Format += new ConvertEventHandler(MyCBBinding_Format);
            printerNumbersCB.DataBindings[0].Parse += new ConvertEventHandler(MyCBBinding_Parse);
            printerDatesCB.DataBindings[0].Format += new ConvertEventHandler(MyCBBinding_Format);
            printerDatesCB.DataBindings[0].Parse += new ConvertEventHandler(MyCBBinding_Parse);
            printerNoDebugCB.DataBindings[0].Format += new ConvertEventHandler(MyCBBinding_Format);
            printerNoDebugCB.DataBindings[0].Parse += new ConvertEventHandler(MyCBBinding_Parse);
            printerSecretTB.TextChanged += new EventHandler(somethingChanged);
            printerEncodingCB.SelectedIndexChanged += new EventHandler(somethingChanged);
            printerNumbersCB.CheckedChanged += new EventHandler(somethingChanged);
            printerDatesCB.CheckedChanged += new EventHandler(somethingChanged);
            printerNoDebugCB.CheckedChanged += new EventHandler(somethingChanged);
            //
            // Progress form
            //
            this.progressForm = new ProgressForm();
            this.progressForm.Canceled += new EventHandler<EventArgs>(buttonCancelProgress_Click);
            this.progressForm.Done += new EventHandler<EventArgs>(buttonDoneProgress_Click);
            this.progressForm.FormClosing += progressForm_FormClosing;

            string logConfFile = Constants.InstallDir + @"\logConfig.xml";
            if (File.Exists(logConfFile))
            {
                XmlConfigurator.Configure(new System.IO.FileInfo(logConfFile));
            }
            else
            {
                Console.WriteLine("Missing log configuration");
                System.Environment.Exit(1);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                this.eiPrintersKey = Registry.LocalMachine.OpenSubKey(Constants.REGKEY, true);
            }
            catch (System.Security.SecurityException)
            {
                this.eiPrintersKey = Registry.LocalMachine.OpenSubKey(Constants.REGKEY);
                isReadOnly = true;
            }
            if (this.eiPrintersKey == null)
            {
                MessageBox.Show(LocRM.GetString("regNotFound"), LocRM.GetString("errorHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                PopulateDataset();
                this.changed = false;
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (isReadOnly)
            {
                MessageBox.Show(LocRM.GetString("noAdmin"), LocRM.GetString("infoHeader"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Text = this.Text + LocRM.GetString("readOnly");
            }
        }

        private void PopulateDataset()
        {
            RegistryKey printer;
            DataTable printers = printersDataSet.Tables["Printers"];
            DataRow row;
            printers.Rows.Clear();
            foreach (string pr in this.eiPrintersKey.GetSubKeyNames())
            {
                printer = this.eiPrintersKey.OpenSubKey(pr);
                row = printers.NewRow();
                row[Constants.RKNAME] = printer.GetValue(Constants.RKNAME);
                row[Constants.RKSECRET] = printer.GetValue(Constants.RKSECRET);
                row[Constants.RKENCODING] = printer.GetValue(Constants.RKENCODING);
                row[Constants.RKNUMBERS] = printer.GetValue(Constants.RKNUMBERS);
                row[Constants.RKDATES] = printer.GetValue(Constants.RKDATES);
                row[Constants.RKNODEBUG] = printer.GetValue(Constants.RKNODEBUG);
                printers.Rows.Add(row);
            }
            printersDataSet.AcceptChanges();
        }

        #region Checkboxes format and parse
        protected void MyCBBinding_Format(object sender, System.Windows.Forms.ConvertEventArgs e)
        {
            if (e.Value.ToString() == "1")
                e.Value = true;
            else
                e.Value = false;
        }

        protected void MyCBBinding_Parse(object sender, System.Windows.Forms.ConvertEventArgs e)
        {
            if ((bool)e.Value == true)
                e.Value = "1";
            else
                e.Value = "0";
        }
        #endregion

        #region Events
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if(!isReadOnly) progressStart();
            else MessageBox.Show(LocRM.GetString("noAdmin"), LocRM.GetString("infoHeader"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
       
        private void buttonAddPrinter_Click(object sender, EventArgs e)
        {
            string input = string.Empty;
            DialogResult apResult = ShowInputDialog(ref input);
            if (apResult == DialogResult.OK)
            {
                input = input.Trim();
                if (input == string.Empty)
                {
                    MessageBox.Show(LocRM.GetString("noPrinterName"), LocRM.GetString("wrongInputHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!IsASCII(input))
                {
                    MessageBox.Show(LocRM.GetString("onlyLatin"), LocRM.GetString("wrongInputHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        AddNewToDataset(input);
                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show(ex.Message, LocRM.GetString("failureHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (changed)
            {
                DialogResult res = MessageBox.Show(LocRM.GetString("changesLost"), LocRM.GetString("warningHeader"), MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (res == DialogResult.Cancel) return;
            }
            this.eiPrintersKey.Close();
            Application.Exit();

        }

        private void somethingChanged(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            if (c.Focused)
            {
                this.changed = true;
                StatusUpdate(LocRM.GetString("saveChanges"));
            }
        }

        private void buttonDeletePrinter_Click(object sender, EventArgs e)
        {
            int idx = printersBindingSource.IndexOf(printersBindingSource.Current);
            if (idx == -1) return;
            DialogResult res = MessageBox.Show(LocRM.GetString("removePrinter"), LocRM.GetString("warningHeader"), MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (res == DialogResult.Cancel) return;
            printersDataSet.Tables["Printers"].Rows[idx].Delete();
            printersDataSet.Tables["Printers"].AcceptChanges();
            this.changed = true;
            StatusUpdate(LocRM.GetString("saveChanges"));
        }
        #endregion

        private string AddPort(string printername)
        {
            RegistryKey RPTkey = Registry.LocalMachine.OpenSubKey(Constants.RKMONITORPORTS, true);
            int portNumber = RPTkey.SubKeyCount + 1;
            string portname = "RPT" + portNumber.ToString() + ":";
            string installDir = (string)Registry.LocalMachine.OpenSubKey(Constants.RKEINVOICE).GetValue(Constants.RKINSTDIR);
            RegistryKey portkey = RPTkey.CreateSubKey(portname);
            portkey.SetValue("Arguments", printername);
            portkey.SetValue("Command", installDir + @"\EInvoice.exe");
            portkey.SetValue("Description", "EInvoice port" + portNumber.ToString());
            portkey.SetValue("LogFileName", installDir + @"\" + portname.ToLower().Replace(":","") + ".log");
            portkey.SetValue("Printer", printername);
            portkey.SetValue("Delay", 300, RegistryValueKind.DWord);
            portkey.SetValue("LogFileDebug", 1, RegistryValueKind.DWord);
            portkey.SetValue("LogFileUse", 1, RegistryValueKind.DWord);
            portkey.SetValue("Output", 0, RegistryValueKind.DWord);
            portkey.SetValue("PrintError", 0, RegistryValueKind.DWord);
            portkey.SetValue("RunUser", 1, RegistryValueKind.DWord);
            portkey.SetValue("ShowWindow", 2, RegistryValueKind.DWord);
            portkey.Close();
            RPTkey.Close();
            return portname;
        }

        private void AddPrinter(string printername, string portname)
        {
            int exitCode;
            string windowsPath = Environment.GetEnvironmentVariable("windir");
            string args = string.Format("printui.dll,PrintUIEntry /if /f {2}\\inf\\ntprint.inf /m \"Generic / Text Only\" /r {0} /b \"{1}\"", portname, printername, windowsPath);
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = args;
            start.FileName = "rundll32.exe";
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;
            using (Process proc = Process.Start(start))
            {
                proc.WaitForExit();
                exitCode = proc.ExitCode;
            }
            if(exitCode != 0){
                throw new SystemException(LocRM.GetString("excCreate"));
            }
        }

        private void DeletePrinter(string pr)
        {
            int exitCode;
            string args = string.Format("printui.dll,PrintUIEntry /dl /n \"{0}\"", pr);
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = args;
            start.FileName = "rundll32.exe";
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;
            using (Process proc = Process.Start(start))
            {
                proc.WaitForExit();
                exitCode = proc.ExitCode;
            }
            if (exitCode != 0)
            {
                throw new SystemException(LocRM.GetString("excRemove"));
            }
        }

        private void DeletePort(string port)
        {
            RegistryKey RPTkey = Registry.LocalMachine.OpenSubKey(Constants.RKMONITORPORTS, true);
            RPTkey.DeleteSubKey(port);
            RPTkey.Close();
        }

        private void AddNewToDataset(string printername)
        {
            printersBindingSource.EndEdit();
            DataTable printers = printersDataSet.Tables["Printers"];
            DataRow row = printers.NewRow();
            row[Constants.RKNAME] = printername;
            printers.Rows.Add(row);
            printers.AcceptChanges();
            printersDataSet.AcceptChanges();
            this.changed = true;
            printerNamesCB.SelectedValue = printername;
            printerNamesCB.Refresh();
        }
        private bool IsASCII(string input)
        {
            const int MaxAnsiCode = 127;
            return !input.Any(c => c > MaxAnsiCode);
        }

        private void StatusUpdate(string text)
        {
            toolStripStatusLabel1.Text = text;
            statusStrip1.Refresh();
        }

        #region Printer Name Input Dialog
        private DialogResult ShowInputDialog(ref string input)
        {
            System.Drawing.Size size = new System.Drawing.Size(240, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = LocRM.GetString("printerName");

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = LocRM.GetString("ok");
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = LocRM.GetString("cancel");
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;
            inputBox.StartPosition = FormStartPosition.CenterParent;

            DialogResult result = inputBox.ShowDialog(this);
            input = textBox.Text;
            return result;
        }
        #endregion

        #region Change CodePage
        //
        // Change code page of TTY printer
        // Tested xp, 2003, vista, 7 all 32bit
        //
        private const string WIN8VERSION = "6.3.9600.0";
        private const uint dataSize32 = 0x4a4;
        private const uint dataSize64 = 0x4b0;   // TODO: Needs checking

        void ChangeCodePage(string printerName, string codepage)
        {
            if (!(codepage == "737" || codepage == "1253")) return;
            Version win8Version = new Version(WIN8VERSION);
            Version osVersion = Environment.OSVersion.Version;
            if (osVersion.CompareTo(win8Version) == -1 || osVersion.CompareTo(win8Version) == 0)
            {
                IntPtr pHandle;
                PRINTER_DEFAULTS defaults = new PRINTER_DEFAULTS();
                defaults.DesiredAccess = ACCESS_MASK.PRINTER_ACCESS_ADMINISTER;
                OpenPrinter(printerName, out pHandle, defaults);
                if (pHandle.ToString() == "0") return;
                string pValueName = "TTY DeviceConfig";
                uint nSize;
                uint needed = 0;
                uint type = 0;
                byte[] pData;
                byte[] tmp = new byte[1];
                GetPrinterData(pHandle, pValueName, out type, tmp, 1, out needed);
                if (needed == 1)
                {
                    if (!Is64BitOperatingSystem()) nSize = dataSize32;
                    else nSize = dataSize64;
                }
                else
                {
                    nSize = needed;
                }

                pData = new byte[nSize];
                GetPrinterData(pHandle, pValueName, out type, pData, nSize, out needed);

                if (codepage == "737") set737(ref pData);
                else set1253(ref pData);
                SetPrinterData(pHandle, pValueName, REGISTRY_TYPES.REG_BINARY, pData, nSize);
                ClosePrinter(pHandle);
            }
            else
            {
                log.Debug(osVersion.Major + "." + osVersion.Minor + "." + osVersion.Build + "." + osVersion.Revision);
                log.Debug(osVersion.ToString());
            }
        }

        void set737(ref byte[] pd)
        {
            pd[0] = 0x08;
            pd[1] = 0x00;
            pd[2] = 0x00;
            pd[3] = 0x00;
            pd[4] = 0xff;
            pd[5] = 0xff;
            pd[6] = 0xff;
            pd[7] = 0xff;
        }

        void set1253(ref byte[] pd)
        {
            pd[0] = 0x08;
            pd[1] = 0x00;
            pd[2] = 0x00;
            pd[3] = 0x00;
            pd[4] = 0xe5;
            pd[5] = 0x04;
            pd[6] = 0x00;
            pd[7] = 0x00;
        }

        [DllImport("winspool.drv", CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern unsafe bool OpenPrinter(string pPrinterName, out IntPtr phPrinter, PRINTER_DEFAULTS pDefault);

        [DllImport("winspool.drv", CharSet = CharSet.Auto)]
        static extern uint GetPrinterData(IntPtr hPrinter, string pValueName, out uint pType, byte[] pData, uint nSize, out uint pcbNeeded);

        [DllImport("winspool.drv", CharSet = CharSet.Auto)]
        static extern uint SetPrinterData(IntPtr hPrinter, string pValueName, uint Type, byte[] pData, uint cbData);

        [DllImport("winspool.drv")]
        static extern int ClosePrinter(IntPtr hPrinter);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        class PRINTER_DEFAULTS
        {
            public IntPtr pDatatype;
            public IntPtr pDevMode;
            public ACCESS_MASK DesiredAccess;
        }

        [Flags]
        enum ACCESS_MASK : uint
        {
            DELETE = 0x00010000,
            READ_CONTROL = 0x00020000,
            WRITE_DAC = 0x00040000,
            WRITE_OWNER = 0x00080000,
            SYNCHRONIZE = 0x00100000,

            STANDARD_RIGHTS_REQUIRED = 0x000F0000,

            STANDARD_RIGHTS_READ = 0x00020000,
            STANDARD_RIGHTS_WRITE = 0x00020000,
            STANDARD_RIGHTS_EXECUTE = 0x00020000,

            STANDARD_RIGHTS_ALL = 0x001F0000,

            SPECIFIC_RIGHTS_ALL = 0x0000FFFF,

            ACCESS_SYSTEM_SECURITY = 0x01000000,

            MAXIMUM_ALLOWED = 0x02000000,

            GENERIC_READ = 0x80000000,
            GENERIC_WRITE = 0x40000000,
            GENERIC_EXECUTE = 0x20000000,
            GENERIC_ALL = 0x10000000,

            DESKTOP_READOBJECTS = 0x00000001,
            DESKTOP_CREATEWINDOW = 0x00000002,
            DESKTOP_CREATEMENU = 0x00000004,
            DESKTOP_HOOKCONTROL = 0x00000008,
            DESKTOP_JOURNALRECORD = 0x00000010,
            DESKTOP_JOURNALPLAYBACK = 0x00000020,
            DESKTOP_ENUMERATE = 0x00000040,
            DESKTOP_WRITEOBJECTS = 0x00000080,
            DESKTOP_SWITCHDESKTOP = 0x00000100,

            WINSTA_ENUMDESKTOPS = 0x00000001,
            WINSTA_READATTRIBUTES = 0x00000002,
            WINSTA_ACCESSCLIPBOARD = 0x00000004,
            WINSTA_CREATEDESKTOP = 0x00000008,
            WINSTA_WRITEATTRIBUTES = 0x00000010,
            WINSTA_ACCESSGLOBALATOMS = 0x00000020,
            WINSTA_EXITWINDOWS = 0x00000040,
            WINSTA_ENUMERATE = 0x00000100,
            WINSTA_READSCREEN = 0x00000200,

            WINSTA_ALL_ACCESS = 0x0000037F,

            PRINTER_ACCESS_ADMINISTER = 0x00000004
        }

        class REGISTRY_TYPES
        {
            public static uint REG_NONE = 0;
            public static uint REG_SZ = 1;
            public static uint REG_EXPAND_SZ = 2;
            public static uint REG_BINARY = 3;
            public static uint REG_DWORD = 4;
            public static uint REG_DWORD_LITTLE_ENDIAN = 4;
            public static uint REG_DWORD_BIG_ENDIAN = 5;
            public static uint REG_LINK = 6;
            public static uint REG_MULTI_SZ = 7;
            public static uint REG_RESOURCE_LIST = 8;
            public static uint REG_FULL_RESOURCE_DESCRIPTOR = 9;
            public static uint REG_RESOURCE_REQUIREMENTS_LIST = 10;
            public static uint REG_QWORD = 11;
            public static uint REG_QWORD_LITTLE_ENDIAN = 11;
        }
        #endregion

        #region Get OS bits
        private static bool Is64BitOperatingSystem()
        {
            if (IntPtr.Size == 8)  // 64-bit programs run only on Win64
            {
                return true;
            }
            else  // 32-bit programs run on both 32-bit and 64-bit Windows
            {
                // Detect whether the current process is a 32-bit process 
                // running on a 64-bit system.
                bool flag;
                return ((DoesWin32MethodExist("kernel32.dll", "IsWow64Process") &&
                    IsWow64Process(GetCurrentProcess(), out flag)) && flag);
            }
        }
        static bool DoesWin32MethodExist(string moduleName, string methodName)
        {
            IntPtr moduleHandle = GetModuleHandle(moduleName);
            if (moduleHandle == IntPtr.Zero)
            {
                return false;
            }
            return (GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
        }
        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentProcess();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr GetProcAddress(IntPtr hModule,
            [MarshalAs(UnmanagedType.LPStr)]string procName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);
        #endregion

        #region Progress form & background worker
        //
        // Progress form & background worker
        //
        private void progressStart()
        {
            this.progressForm.buttonCancel.Visible = true;
            this.progressForm.buttonDone.Visible = false;
            this.progressForm.Message = LocRM.GetString("inProgress");
            this.progressForm.StartPosition = FormStartPosition.CenterParent;
            this.processedRows = 0.0F;
            this.backgroundWorker1.RunWorkerAsync();
            this.progressForm.ShowDialog(this);
        }

        private void buttonCancelProgress_Click(object sender, EventArgs e)
        {
            if (this.backgroundWorker1.IsBusy)
            {
                this.backgroundWorker1.CancelAsync();
                this.progressForm.Hide();
            }
        }

        private void buttonDoneProgress_Click(object sender, EventArgs e)
        {
            this.progressForm.Hide();
        }

        private void progressForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this.backgroundWorker1.CancellationPending)
            {
                this.backgroundWorker1.CancelAsync();
            }
            e.Cancel = true;
            this.progressForm.Hide();
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            DataTable printers = printersDataSet.Tables["Printers"];
            RegistryKey printer;
            string[] regPrinters = this.eiPrintersKey.GetSubKeyNames();
            int totalRows = printers.Rows.Count + regPrinters.Count();
            BackgroundWorker worker = sender as BackgroundWorker;
            try
            {
                foreach (DataRow row in printers.Rows)
                {
                    if (worker.CancellationPending == true)
                    {
                        e.Cancel = true;
                        break;
                    }
                    else
                    {
                        if (this.eiPrintersKey.GetSubKeyNames().Contains(row[Constants.RKNAME]))
                        {
                            printer = this.eiPrintersKey.OpenSubKey((string)row[Constants.RKNAME], true);
                        }
                        else
                        {
                            string port = AddPort((string)row[Constants.RKNAME]);
                            AddPrinter((string)row[Constants.RKNAME], port);
                            printer = this.eiPrintersKey.CreateSubKey((string)row[Constants.RKNAME]);
                            printer.SetValue(Constants.RKNAME, (string)row[Constants.RKNAME]);
                            printer.SetValue(Constants.RKPORT, port);
                            printer.SetValue(Constants.EISTART, Constants.EISTARTVAL);
                            printer.SetValue(Constants.EIEND, Constants.EIENDVAL);
                        }
                        printer.SetValue(Constants.RKSECRET, (string)row[Constants.RKSECRET]);
                        printer.SetValue(Constants.RKENCODING, (string)row[Constants.RKENCODING]);
                        printer.SetValue(Constants.RKDATES, (string)row[Constants.RKDATES]);
                        printer.SetValue(Constants.RKNUMBERS, (string)row[Constants.RKNUMBERS]);
                        printer.SetValue(Constants.RKNODEBUG, (string)row[Constants.RKNODEBUG]);
                        printer.Close();
                        // ChangeCodePage((string)row[Constants.RKNAME], (string)row[Constants.RKENCODING]);
                        processedRows += 1.0F;
                        worker.ReportProgress((int)((processedRows / totalRows) * 100));
#if DEBUG
                        System.Threading.Thread.Sleep(1000);
#endif
                    }
                }
                foreach (string pr in regPrinters)
                {
                    if (worker.CancellationPending == true || e.Cancel == true)
                    {
                        e.Cancel = true;
                        break;
                    }
                    else
                    {
                        bool found = false;
                        foreach (DataRow row in printers.Rows)
                        {
                            if (pr.Contains((string)row[Constants.RKNAME]))
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            printer = this.eiPrintersKey.OpenSubKey(pr);
                            string port = (string)printer.GetValue("Port");
                            this.eiPrintersKey.DeleteSubKey(pr);
                            DeletePrinter(pr);
                            DeletePort(port);
                        }
                        processedRows += 1.0F;
                        worker.ReportProgress((int)((processedRows / totalRows) * 100));
#if DEBUG
                        System.Threading.Thread.Sleep(1000);
#endif
                    }
                }
            }
            catch (SystemException ex)
            {
                e.Cancel = true;
                MessageBox.Show(ex.Message, LocRM.GetString("errorHeader"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            this.progressForm.Message = LocRM.GetString("inProgress") + e.ProgressPercentage.ToString() + "%";
            this.progressForm.ProgressValue = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            PopulateDataset();
            this.changed = false;
            if (!e.Cancelled)
            {
                this.progressForm.buttonCancel.Visible = false;
                this.progressForm.buttonDone.Visible = true;
                this.progressForm.Message = LocRM.GetString("saveSuccess");
            }
            else
            {
                this.progressForm.Hide();
            }
        }

        #endregion
    }
}
