﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DreamSolutions.EInvoicePrinters
{
    public partial class ProgressForm : Form
    {
        public event EventHandler<EventArgs> Canceled;

        public event EventHandler<EventArgs> Done;

        public event EventHandler<EventArgs> ClosingForm;

        public string Message
        {
            set { labelMessage.Text = value; }
        }

        public int ProgressValue
        {
            set { progressBar1.Value = value; }
        }

        public ProgressForm()
        {
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            EventHandler<EventArgs> ea = Canceled;
            if (ea != null) ea(this, e);
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            EventHandler<EventArgs> ea = Done;
            if (ea != null) ea(this, e);
        }

        private void ProgressForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            EventHandler<EventArgs> ea = ClosingForm;
            if (ea != null) ea(this, (EventArgs)e);
        }
    }
}
