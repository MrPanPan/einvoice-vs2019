﻿using System;
using System.Runtime.InteropServices;
using DreamSolutions.EInvoice;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace DreamSolutions.EInvoicePrinters
{
    public delegate void FirstTimeCaller(Constants constants, out RegistryKey rk);
    public class FirstTime
    {
        public void Create(Constants constants, out RegistryKey rk)
        {
            int exitCode;
            ProcessStartInfo start = new ProcessStartInfo();
            start.Arguments = "stop spooler";
            start.FileName = "net";
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;
            using (Process proc = Process.Start(start))
            {
                proc.WaitForExit();
                exitCode = proc.ExitCode;
            }

            string installDir = new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName;
            RegistryKey rkEI = Registry.LocalMachine.CreateSubKey(constants.RKEINVOICE, RegistryKeyPermissionCheck.ReadWriteSubTree);
            rkEI.SetValue(constants.RKINSTDIR, installDir);
            rk = Registry.LocalMachine.CreateSubKey(constants.REGKEY, RegistryKeyPermissionCheck.ReadWriteSubTree);
            RegistryKey rkMonitor = Registry.LocalMachine.CreateSubKey(constants.RKMONITOR, RegistryKeyPermissionCheck.ReadWriteSubTree);

            if (Is64BitOperatingSystem()) rkMonitor.SetValue("Driver", installDir + @"\x64\redmon64.dll");
            else rkMonitor.SetValue("Driver", installDir + @"\x86\redmon32.dll");

            RegistryKey rkMonitorPorts = Registry.LocalMachine.CreateSubKey(constants.RKMONITORPORTS, RegistryKeyPermissionCheck.ReadWriteSubTree);
            rkMonitorPorts.Close();
            rkMonitor.Close();
            rkEI.Close();
            start.Arguments = "start spooler";
            using (Process proc = Process.Start(start))
            {
                proc.WaitForExit();
                exitCode = proc.ExitCode;
            }
        }


        private static bool Is64BitOperatingSystem()
        {
            if (IntPtr.Size == 8)  // 64-bit programs run only on Win64
            {
                return true;
            }
            else  // 32-bit programs run on both 32-bit and 64-bit Windows
            {
                // Detect whether the current process is a 32-bit process 
                // running on a 64-bit system.
                bool flag;
                return ((DoesWin32MethodExist("kernel32.dll", "IsWow64Process") &&
                    IsWow64Process(GetCurrentProcess(), out flag)) && flag);
            }
        }
        static bool DoesWin32MethodExist(string moduleName, string methodName)
        {
            IntPtr moduleHandle = GetModuleHandle(moduleName);
            if (moduleHandle == IntPtr.Zero)
            {
                return false;
            }
            return (GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
        }
        [DllImport("kernel32.dll")]
        static extern IntPtr GetCurrentProcess();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr GetModuleHandle(string moduleName);

        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr GetProcAddress(IntPtr hModule,
            [MarshalAs(UnmanagedType.LPStr)]string procName);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);
    }
}
