﻿using System;
using System.Linq;
using System.IO;
using log4net;
using log4net.Config;
using log4net.Core;
using System.Threading;


namespace DreamSolutions.EInvoice
{
    class EInvoiceMain
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EInvoiceMain));

        static void Main(string[] args)
        {
            string logConfFile = Constants.InstallDir + @"\logConfig.xml";
            if (File.Exists(logConfFile))
            {
                XmlConfigurator.Configure(new System.IO.FileInfo(logConfFile));
            }
            else
            {
                Console.WriteLine("Missing log configuration");
                System.Environment.Exit(1);
            }

            
            if (args.Any())
            {
                Thread mainThread = new System.Threading.Thread(() => StartThreads(args[0]));
                log.Debug("StartThread starting");
                mainThread.Start();
                mainThread.Join();
                log.Debug("StartThread joined");
            }
            else
            {
                //Microsoft.VisualBasic.Interaction.MsgBox("Incorrect eINVOICE port setup");
                log.Error("No port given");
                System.Environment.Exit(1);
            }
            System.Environment.Exit(0);
        }
        static void StartThreads(string arg)
        {
            EInvoiceObject eio = null;
            log.Debug("Entered StartThread");
            try
            {
                
#if DEBUG
                eio = new EInvoiceObject("");
                eio.ParseInput(File.Open(arg, FileMode.Open, FileAccess.Read));
#else
                eio = new EInvoiceObject(arg);
                eio.ParseInput(Console.OpenStandardInput());
#endif
                string ret = eio.SendIt();
                if (ret != "")
                    Microsoft.VisualBasic.Interaction.MsgBox(ret);
            }
            catch (SystemException e)
            {
                if (eio != null && !eio.nodebug)
                    Microsoft.VisualBasic.Interaction.MsgBox(e.Message);
                log.Error(e.Message.Replace("\r", " ").Replace("\n", " "));
            }
            log.Debug("Leaving StartThread");
        }
    }
}
