﻿using Microsoft.Win32;
using System.Diagnostics;
using System.IO;
namespace DreamSolutions.EInvoice
{
    public static class Constants
    {
        public static string REGKEY = @"Software\DreamSolutions\eINVOICEGlobalClient\Printers";
        public static string RKEINVOICE = @"Software\DreamSolutions\eINVOICEGlobalClient";
        public static string RKNAME = "Name";
        public static string RKSECRET = "Secret";
        public static string RKENCODING = "Encoding";
        public static string RKNUMBERS = "GrNumbers";
        public static string RKDATES = "GrDates";
        public static string RKNODEBUG = "NoDebug";
        public static string RKPORT = "Port";
        public static string RKEIURL = "WebApiUrl";
        public static string RKMONITORPORTS = @"System\CurrentControlSet\Control\Print\Monitors\Redirected Port\Ports";
        public static string RKMONITOR = @"System\CurrentControlSet\Control\Print\Monitors\Redirected Port";
        public static string RKINSTDIR = "InstallDir";                 // Installdir reg name
        public static string SENDMETHOD = "E";                         // Send metod
        public static string EIURL = "https://einvoiceapp.softonecloud.com/"; // S1 eINVOICE WEBAPI url
        public static string EIINFO = "DREAM SOLUTIONS IKE";           // Customer info
        public static string EISTART = "EIstart";                      // Start tag reg name
        public static string EIEND = "EIend";                          // End tag reg name
        public static string EISTARTVAL = "<<";                        // Start tag
        public static string EIENDVAL = ">>";                          // End tag

        public static int SIGNCOLUMN = 6;                              // The index of sign column

        public static string InstallDir
        {                              // Get install dir form reg
            get {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    return Directory.GetCurrentDirectory();
                }
                else
                {
                    return (string)Registry.LocalMachine.OpenSubKey(RKEINVOICE).GetValue(RKINSTDIR);
                }
            }
        }

        public static string WebApiUrl
        {                              // Get WEBAPI URL
            get
            {
                    return (string)Registry.LocalMachine.OpenSubKey(RKEINVOICE).GetValue(RKEIURL);
            }            
        }

        public static string SQLiteConString
        {              
            get
            {
                return string.Format(@"Data Source={0}\{1};Version=3;", Constants.InstallDir, "einvoicedb.sqlite");
            }
        }
    }
}