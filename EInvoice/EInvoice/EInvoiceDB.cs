﻿using System.Configuration;
using System.Data.Common;
using log4net;

namespace DreamSolutions.EInvoice
{
    public class EInvoiceDB
    {
        public string DBProvider;
        public string ConString;
        public DbProviderFactory factory;
        public DbConnection connection;

        protected internal Configuration config;
        private static readonly ILog log = LogManager.GetLogger(typeof(EInvoiceDB));

        public EInvoiceDB()
        {
            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
            configMap.ExeConfigFilename = Constants.InstallDir + @"\EInvoice.exe.config";
            config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            this.DBProvider = config.AppSettings.Settings["EIDBProvider"].Value;
            this.ConString = GetConnectionStringByProvider(this.DBProvider);
            this.factory = DbProviderFactories.GetFactory(this.DBProvider);
            log.Debug("ConString: "+this.ConString);
            log.Debug("Provider: "+this.DBProvider);
        }

        public DbConnection GetConnection()
        {
            this.connection = this.factory.CreateConnection();
            this.connection.ConnectionString = this.ConString;
            return this.connection;
        }

        private string GetConnectionStringByProvider(string providerName)
        {
            if (providerName.Contains("SQLite"))
            {
                return Constants.SQLiteConString;
            }
            string returnValue = null;
            ConnectionStringSettingsCollection settings = config.ConnectionStrings.ConnectionStrings;

            if (settings != null)
            {
                foreach (ConnectionStringSettings cs in settings)
                {
                    if (cs.ProviderName == providerName)
                    {
                        returnValue = cs.ConnectionString;
                        break;
                    }
                }
            }
            return returnValue;
        }
    }
}
