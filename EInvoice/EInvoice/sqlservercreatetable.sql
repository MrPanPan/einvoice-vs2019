CREATE TABLE einvoicelog (
	id		INT primary key not null identity(1,1),
	printer		nvarchar(255),
	invID		nvarchar(255),
	dateSend	DATETIME,
	invDate		DATETIME,
	invSeries	nvarchar(255),
	invNumber	nvarchar(255),
	status		INT,
	sign		nvarchar(255),
	updated 	INT
);
create unique index sign_idx on einvoicelog(sign);
create index dateSend_idx on einvoicelog(dateSend DESC);
create index invDate_idx on einvoicelog(invDate);
create index invNumber_idx on einvoicelog(invNumber);