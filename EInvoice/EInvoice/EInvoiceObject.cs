﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using DreamSolutions.EInvoiceDataSet;
using DreamSolutions.EIUpdateSign;
using Microsoft.Win32;
using eINVOICEWebApiClient;
using log4net;
using System.Threading;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Net;
using System.Resources;
using System.Windows.Forms;


namespace DreamSolutions.EInvoice
{
    public class EInvoiceObject
    {
        string eiSecret;
        string eiEncoding;
        bool grNumbers;
        bool grDates;
        //Constants constants = new Constants();
        string eiStart = string.Empty;
        string eiEnd = string.Empty;
        string printerName;
        string[] connectors;
        string afm;
        string country;
        bool isNew = true;
        public bool nodebug;
        string formatted = string.Empty;
        EIDataSet EIds;
        EInvoiceDB eiDB;
        DbConnection sql_con;
        DbCommand cmd;
        WebApiClient wac;
        WebApiClient.SendDocumentResponse response;
        int DBThreadTime;
        int UploadThreadTime;
        private static readonly ILog log = LogManager.GetLogger(typeof(EInvoiceObject));
        private static ResourceManager LocRM = new ResourceManager("DreamSolutions.EInvoice.EinvoiceStrings", typeof(EInvoice.EInvoiceObject).Assembly);

        public EInvoiceObject(string printer)
        {
#if DEBUG
            if (printer == string.Empty) this.printerName = "DEBUGPRINTER";
            else this.printerName = printer;
            this.eiSecret = "DEBUGSECRET";
            this.eiEncoding = "1253";
            this.grNumbers = true;
            this.grDates = true;
            this.eiStart = "<<";
            this.eiEnd = ">>";
            this.nodebug = false;
#else
            RegistryKey eiPrintersKey = Registry.LocalMachine.OpenSubKey(Constants.REGKEY);
            if (eiPrintersKey == null)
            {
                throw new SystemException("Non existent eINVOICE printers key in registry");
            }
            bool found = false;
            foreach (string pr in eiPrintersKey.GetSubKeyNames())
            {
                if (pr == printer)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                throw new SystemException(string.Format("Printer {0} not in registry", printer));
            }
            RegistryKey eiPrinterKey = eiPrintersKey.OpenSubKey(printer);
            this.printerName = (string)eiPrinterKey.GetValue(Constants.RKNAME, string.Empty);
            if (this.printerName == string.Empty)
            {
                throw new SystemException("No printer name set");
            }
            this.eiSecret = (string)eiPrinterKey.GetValue(Constants.RKSECRET, string.Empty);
            if (this.eiSecret == string.Empty)
            {
                throw new SystemException("No secret set");
            }
            this.eiEncoding = (string)eiPrinterKey.GetValue(Constants.RKENCODING, "1253");
            this.grNumbers = (string)eiPrinterKey.GetValue(Constants.RKNUMBERS, "1") == "1" ? true : false;
            this.grDates = (string)eiPrinterKey.GetValue(Constants.RKDATES, "1") == "1" ? true : false;
            this.eiStart = (string)eiPrinterKey.GetValue(Constants.EISTART, "<<");
            this.eiEnd = (string)eiPrinterKey.GetValue(Constants.EIEND, ">>");
            this.nodebug = (string)eiPrinterKey.GetValue(Constants.RKNODEBUG, "1") == "1" ? true : false;
#endif
            try
            {
                this.connectors = ConfigurationManager.AppSettings["ERPConnectors"].Split(',');
            }
            catch (SystemException)
            {
                this.connectors = new string[0];
            }

            this.eiDB = new EInvoiceDB();
            this.sql_con = this.eiDB.GetConnection();
            log4net.ThreadContext.Properties["printer"] = this.printerName;
            try
            {
                DBThreadTime = Convert.ToInt32(ConfigurationManager.AppSettings["DBThreadTime"]);
            }
            catch (SystemException)
            {
                DBThreadTime = 60;
            }
            try
            {
                UploadThreadTime = Convert.ToInt32(ConfigurationManager.AppSettings["UploadThreadTime"]);
            }
            catch (SystemException)
            {
                UploadThreadTime = 240;
            }
            log.Debug("EIObject created: Encoding: " + this.eiEncoding + ", grNumbers: " + this.grNumbers.ToString() + ", grDates: " + this.grDates.ToString());
        }

        public void ParseInput(Stream inputStream)
        {
            log.Debug("Entering ParseInput");
            List<byte> inputList = new List<byte>();
            Int32 inByte = 0;
            while (inByte != -1)
            {
                inByte = inputStream.ReadByte();
                if (inByte >= 32)
                {
                    inputList.Add((byte)inByte);
                }
            }
            byte[] bytes2 = { };
            if (this.eiEncoding == "1253")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(1253), Encoding.UTF8, inputList.ToArray());
            }
            else if (this.eiEncoding == "737")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(737), Encoding.UTF8, inputList.ToArray());
            }
            else
            {
                throw new SystemException("Wrong encoding\r\nAccepted encodings: 1253, 737");
            }
            string inputEInvoice = Encoding.UTF8.GetString(bytes2)
                                    .Replace(this.eiStart, "{{")
                                    .Replace(this.eiEnd, "}}")
                                    .Replace("&", "&amp;")
                                    .Replace("<", "&lt;")
                                    .Replace(">", "&gt;")
                                    .Replace("\"", "&quot;")
                                    .Replace("'", "&apos;")
                                    .Replace("{{", "<")
                                    .Replace("}}", ">");
            EIDataSet eids = new EIDataSet();
            try
            {
                using (Stream s = GenerateStreamFromString(inputEInvoice))
                {
                    eids.ReadXml(s, XmlReadMode.IgnoreSchema);
                }
            }
            catch (System.Xml.XmlException e)
            {
                throw new SystemException("Parsing error: " + e.Message);
            }
            if(log.IsDebugEnabled)
                eids.WriteXml(Constants.InstallDir + "/beforefix.xml");
            try
            {
                eids.fixFields(this.grNumbers, this.grDates);
            }
            catch (SystemException e)
            {
                throw new SystemException("Fixing fields error: " + e.Message);
            }
            if (log.IsDebugEnabled)
                eids.WriteXml(Constants.InstallDir + "/afterfix.xml");
            this.EIds = eids;
            this.afm = (string)this.EIds.Tables["H"].Rows[0]["f27"];
            try
            {
                this.country = (string)this.EIds.Tables["H"].Rows[0]["f37"];
            }
            catch (SystemException)
            {
                this.country = string.Empty;
            }
            log.Debug("AFM: " + this.afm);
            log.Debug("Leaving ParseInput");
        }

        #region CustomCheck

        string CustomJson(string api_key, string afm, string[] connectors, string version, string country)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.WriteStartObject();
                writer.WritePropertyName("api_key");
                writer.WriteValue(api_key);
                writer.WritePropertyName("afm");
                writer.WriteValue(afm);
                writer.WritePropertyName("version");
                writer.WriteValue(version);
                writer.WritePropertyName("country");
                writer.WriteValue(country);
                writer.WritePropertyName("connectors");
                writer.WriteStartArray();
                foreach (string connector in connectors)
                {
                    writer.WriteValue(connector);
                }
                writer.WriteEnd();
                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        void CustomCheck()
        {
#if DEBUG
            string url = "http://192.168.2.25:8000/einvoice/check_customer/";
#else
            string url = "http://custom.dream-solutions.gr/einvoice/check_customer/";
#endif
            string json = CustomJson(this.eiSecret, this.afm, this.connectors, Application.ProductVersion, this.country);
            log.Debug("CustomCheck: " + url);
            log.Debug("CustomCheck: " + json);
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string result;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                log.Debug("CustomCheck: " + result);
                JObject jo = JObject.Parse(result);
                this.isNew = (bool)jo.GetValue("new");
                JArray ja = (JArray)jo.SelectToken("connectors");
                this.connectors = ja.Select(jv => (string)jv).ToArray();
            }
            catch (WebException ex)
            {
                string response = string.Empty;
                if (ex.Response != null)
                {
                    using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        response = streamReader.ReadToEnd();
                    }
                }
                log.Error(ex.Message.Replace("\r", " ").Replace("\n", " ") + ": " + response.Replace("\r", " ").Replace("\n", " "));
            }
            catch (SystemException ex2)
            {
                log.Error(ex2.Message.Replace("\r", " ").Replace("\n", " "));
            }
        }

        #endregion

        public string SendIt()
        {
            CustomCheck();
            log.Debug("Entering SendIt");
            //this.wac = new WebApiClient(Constants.EIURL, this.EIds.getVersion());
            this.wac = new WebApiClient(Constants.WebApiUrl, this.EIds.getVersion());
#if DEBUG
            this.response = new WebApiClient.SendDocumentResponse();
            response.Status = WebApiClient.StatusEnum.OK;
            DateTime centuryBegin = new DateTime(2001, 1, 1);
            DateTime currentDate = DateTime.Now;
            long elapsedTicks = currentDate.Ticks - centuryBegin.Ticks;
            response.Sign = elapsedTicks.ToString();
            this.EIds.ToFormat(wac, Constants.InstallDir + "/beforeToFormat.xml");
#else
            this.response = wac.SendDocument(this.eiSecret, Constants.SENDMETHOD, Constants.EIINFO, this.EIds.ToFormat(wac, Constants.InstallDir + "/beforeToFormat.xml"));
#endif
            string ret;
            if (response.Status == WebApiClient.StatusEnum.OK)
            {
                Thread threadDB = new Thread(SaveToDbAndUpdateSign);
                Thread threadUpload = new Thread(UploadAttachment);
                log.Debug("Starting threadDB, threadUpload");
                threadDB.Start();
                threadUpload.Start();
                if (this.nodebug)
                {
                    log.Info(string.Format("Successful registration. Sign: {0}", response.Sign));
                    ret = "";
                }
                else
                {
                    string tmp = LocRM.GetString("msgSuccess");
                    ret = LocRM.GetString("msgSuccess") + "\r\n" + string.Format(LocRM.GetString("msgSign"), response.Sign);
                }
                threadDB.Join(DBThreadTime * 1000);
                threadUpload.Join(UploadThreadTime * 1000);
                log.Debug("Joined threadDB, threadUpload");
            }
            else if (response.Status == WebApiClient.StatusEnum.WARNING)
            {

                if (this.nodebug)
                {
                    log.Warn(string.Format("Warning: {0} Sign: {1}", response.Message, response.Sign));
                    ret = "";
                }
                else
                {
                    ret = LocRM.GetString("msgWarning") + response.Message + "\r\n" + string.Format(LocRM.GetString("msgSign"), response.Sign);
                }
            }
            else
            {
                if (this.nodebug)
                {
                    log.Error(string.Format("Error in registration: {0}", response.Message));
                    ret = "";
                }
                else
                {
                    ret = LocRM.GetString("msgError") + "\r\n" + response.Message;
                }
            }
            log.Debug("Leaving SendIt");
            return ret;
        }

        private void SaveToDbAndUpdateSign()
        {
            log4net.ThreadContext.Properties["printer"] = this.printerName;
            SaveToDb();
            UpdateSign();
        }

        public void UpdateSign()
        {
            string id = null;
            try
            {
                id = this.EIds.Tables["DS"].Rows[0]["f1"].ToString();
            }
            catch (SystemException) {}
            if (id != null)
            {
                UpdateSign(id, this.response.Sign);
            }
        }

        public void UpdateSign(string id, string sign)
        {
            log.Debug("Entering UpdateSign");
            EIUS eius = new EIUS(id, sign, this.printerName, Constants.InstallDir, this.connectors);
            try
            {
                if (eius.DoUpdate())
                {
                    this.cmd = this.sql_con.CreateCommand();
                    this.sql_con.Open();
                    try
                    {
                        using (var sqlTransaction = this.sql_con.BeginTransaction())
                        {
                            this.cmd.CommandText = string.Format("UPDATE einvoicelog SET updated=1 where sign='{0}'",
                                    sign
                                    );
                            this.cmd.Transaction = sqlTransaction;
                            int affected = this.cmd.ExecuteNonQuery();
                            sqlTransaction.Commit();
                            if(affected == 1) log.Info(string.Format("Updated log row: Invoice ID {0}, Sign {1}",id, sign));
                            else log.Info(string.Format("Log row not updated: Invoice ID {0}, sign {1}", id, sign));
                        }
                    }
                    catch (SystemException ex)
                    {
                        log.Error("UPDATE SIGN Error: " + ex.Message.Replace("\r", " ").Replace("\n", " "));
                    }
                    finally
                    {
                        this.cmd.Dispose();
                        this.sql_con.Close();
                        log.Debug("UpdateSign finally");
                    }
                }
                else
                {
                    log.Error(string.Format("ERPConnector Error: Sign not updated to: Invoice ID {0}, Sign {1}", id, sign));
                }
            }
            catch (NotImplementedException) { log.Debug("Not implemented"); }
            catch (SystemException ex)
            {
                log.Error("ERPConnector Error: " + ex.Message.Replace("\r", " ").Replace("\n", " "));
            }
            log.Debug("Leaving UpdateSign");
        }

        private void SaveToDb()
        {
            log.Debug("Entering SaveToDb");
            this.cmd = this.sql_con.CreateCommand();
            this.sql_con.Open();
            int affected = 0;
            try
            {
                using (var sqlTransaction = this.sql_con.BeginTransaction())
                {
                    string invid, invdate, invseries, invnumber;
                    try
                    {
                        invid = this.EIds.Tables["DS"].Rows[0]["f1"].ToString();
                    }
                    catch (SystemException)
                    {
                        invid = "";
                    }
                    try
                    {
                        invdate = this.EIds.Tables["H"].Rows[0]["f21"].ToString();
                    }
                    catch (SystemException)
                    {
                        invdate = "";
                    }
                    try
                    {
                        invseries = this.EIds.Tables["H"].Rows[0]["f12"].ToString();
                    }
                    catch (SystemException)
                    {
                        invseries = "";
                    }
                    try
                    {
                        invnumber = this.EIds.Tables["H"].Rows[0]["f13"].ToString();
                    }
                    catch (SystemException)
                    {
                        invnumber = "";
                    }
                    this.cmd.CommandText = string.Format("INSERT INTO einvoicelog (printer, invID, dateSend, invDate, invSeries, invNumber, status, sign) values('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6}, '{7}')",
                        this.printerName,
                        invid,
                        DateTime.Now.ToString("s"),
                        invdate,
                        invseries,
                        invnumber,
                        1,
                        this.response.Sign
                        );
                    this.cmd.Transaction = sqlTransaction;
                    affected = this.cmd.ExecuteNonQuery();
                    sqlTransaction.Commit();
                    if (affected == 1) log.Debug(string.Format("INSERTED 1 row: Invoice ID {0}, Sign {1}", invid, this.response.Sign));
                    else log.Debug(string.Format("No row inserted: Invoice ID {0}, Sign {1}", invid, this.response.Sign));
                }
            }
            catch (SystemException ex)
            {
                log.Error("INSERT Error: " + ex.Message.Replace("\r", " ").Replace("\n", " "));
            }
            finally
            {
                this.cmd.Dispose();
                this.sql_con.Close();
                log.Debug("SaveToDb Finally");
            }
            log.Debug("Leaving SaveToDb");
        }

        private void UploadAttachment()
        {
            log4net.ThreadContext.Properties["printer"] = this.printerName;
            log.Debug("Entering UploadAttachment");
            string filePath, description;
            int daysToLive;
            bool snailMail;
            try
            {
                filePath = this.EIds.Tables["DS"].Rows[0]["f50"].ToString();
                description = this.EIds.Tables["DS"].Rows[0]["f51"].ToString();
            }
            catch (SystemException)
            {
                filePath = string.Empty;
                description = string.Empty;
            }
            if (!(filePath == string.Empty || description == string.Empty))
            {
                try
                {
                    daysToLive = Convert.ToInt32(this.EIds.Tables["DS"].Rows[0]["f52"].ToString());
                }
                catch (SystemException)
                {
                    daysToLive = 30;
                }
                try
                {
                    snailMail = Convert.ToBoolean(this.EIds.Tables["DS"].Rows[0]["f53"].ToString());
                }
                catch (SystemException)
                {
                    snailMail = false;
                }
                try
                {
#if DEBUG
                    log.Debug("Upload File Success: " + filePath);
#else
                    //this.wac.UploadAttachment(Constants.EIURL, this.eiSecret, this.response.Sign, filePath, description, daysToLive, snailMail);
                    this.wac.UploadAttachment(Constants.WebApiUrl, this.eiSecret, this.response.Sign, filePath, description, daysToLive, snailMail);
                    log.Info("Upload File Success: " + filePath);
#endif
                }
                catch (SystemException ex)
                {
                    log.Error("Upload File Error: " + ex.Message.Replace("\r", " ").Replace("\n", " "));
                }
            }
            log.Debug("Leaving UploadAttachment");
        }
        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
