﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("EIUpdateSign")]
[assembly: AssemblyDescription("Update Sign to ERP")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("DreamSolutions")]
[assembly: AssemblyProduct("EIUpdateSign")]
[assembly: AssemblyCopyright("Copyright © DreamSolutions 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8e8295a5-bd0b-4811-a037-e595d1a9a3e6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.4.68.355")]
[assembly: AssemblyFileVersion("2.4.68.355")]
