﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Configuration;
using System.Reflection;
using System.IO;
using Microsoft.Win32;

namespace DreamSolutions.EIUpdateSign
{
    public class EIUS
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EIUS));

        private string _id;
        private string _sign;
        private string _printerName;
        private string _workingDir;
        private string[] _connectors;
        protected internal Configuration config;

        public EIUS(string id, string sign, string printerName, string workingDir, string[] connectors)
        {
            this._id = id;
            this._sign = sign;
            this._printerName = printerName;
            this._workingDir = workingDir;
            this._connectors = connectors;
            /*
            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
            configMap.ExeConfigFilename = this._workingDir + @"\EInvoice.exe.config";
            config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
            try
            {
                this._connectors = config.AppSettings.Settings["ERPConnectors"].Value.Split(',');
            }
            catch (SystemException)
            {
                this._connectors = new string[0];
            }
             * */

        }
        public bool DoUpdate()
        {
            log.Debug(string.Format("id: {0} sign: {1} printer: {2}", this._id, this._sign, this._printerName));
            log.Debug("Connectors: " + string.Join(" ", this._connectors));
            log.Debug("WorkingDir: " + this._workingDir);
#if DEBUG
            return true;
#else
            foreach (string con in this._connectors)
            {
                object[] parameters = new object[] {this._id, this._sign, this._printerName, this._workingDir};
                Assembly connectorDLL;
                try
                {
                    connectorDLL = Assembly.LoadFile(_workingDir + "\\" + con + ".dll");
                }
                catch (FileNotFoundException)
                {
                    log.Debug("ERPConnector DLL not found");
                    throw new NotImplementedException();
                }
                Type type;
                try
                {
                    type = connectorDLL.GetType("DreamSolutions.ERPConnectors." + con);
                }
                catch (SystemException)
                {
                    log.Debug("ERPConnector exception getting type from DLL");
                    throw new NotImplementedException();
                }
                ConstructorInfo ctor = type.GetConstructor(System.Type.GetTypeArray(parameters));
                if (ctor != null)
                {
                    object connector = ctor.Invoke(parameters);
                    MethodInfo methodInfo = type.GetMethod("DoUpdate");
                    try
                    {
                        bool ret = (bool)methodInfo.Invoke(connector, null);
                        log.Info("ERPConnector result: " + ret.ToString() + string.Format(" Invoice ID {0}, Sign {1}",this._id, this._sign));
                        return ret;
                    }
                    catch (TargetInvocationException ex)
                    {
                        if (ex.GetBaseException().GetType() != typeof(System.NotImplementedException))
                        {
                            throw ex.GetBaseException();
                        }
                    }
                }
            }
            throw new NotImplementedException();
#endif
        }

    }
}
