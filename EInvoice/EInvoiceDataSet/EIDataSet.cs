﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.IO;
using log4net;
using eINVOICEWebApiClient;



//TEST123

namespace DreamSolutions.EInvoiceDataSet
{
    public class EIDataSet: DataSet
    {
        //---------------------------------------------------------------
        // My methods
        //---------------------------------------------------------------
        private string version;
        private string DTOutFormat;
        private string[] XNumbers;
        private string[] TNumbers;
        private string[] DNumbers;
        private string[] HNumbers;
        private string[] DDateTimes;
        private string[] HDateTimes;
        private NumberFormatInfo inNfi;
        private NumberFormatInfo outNfi;
        private DateTimeFormatInfo inDTfi;
        private static readonly ILog log = LogManager.GetLogger(typeof(EIDataSet));

        public EIDataSet()
        {
            Type type = typeof(EIDataSet);
            Assembly ass = type.Assembly;
            this.ReadXmlSchema(ass.GetManifestResourceStream(type.Namespace + ".schema.xsd"));
            this.XNumbers = Properties.Settings.Default.XNumbers.Cast<string>().ToArray();
            this.TNumbers = Properties.Settings.Default.TNumbers.Cast<string>().ToArray();
            this.DNumbers = Properties.Settings.Default.DNumbers.Cast<string>().ToArray();
            this.HNumbers = Properties.Settings.Default.HNumbers.Cast<string>().ToArray();
            this.DDateTimes = Properties.Settings.Default.DDateTimes.Cast<string>().ToArray();
            this.HDateTimes = Properties.Settings.Default.HDateTimes.Cast<string>().ToArray();
            this.DTOutFormat = Properties.Settings.Default.dtOutFormat;
            this.version = Properties.Settings.Default.version;
        }

        public void fixFields(bool grNumbers, bool grDates)
        {
            log.Debug("Entering fixFields");
            string[] empty = { };

            CultureInfo grCulture = new CultureInfo("el-GR");
            CultureInfo usCulture = new CultureInfo("en-US");
            if (grNumbers)
            {
                this.inNfi = grCulture.NumberFormat;
            }
            else
            {
                this.inNfi = usCulture.NumberFormat;
            }
            this.outNfi = usCulture.NumberFormat;
            outNfi.NumberGroupSeparator = "";
            if (grDates)
            {
                this.inDTfi = grCulture.DateTimeFormat;
            }
            else
            {
                this.inDTfi = usCulture.DateTimeFormat;
            }
            fixTable(this.Tables["X"], this.XNumbers, empty);
            fixTable(this.Tables["T"], this.TNumbers, empty);
            fixTable(this.Tables["A"], empty, empty);
            fixTable(this.Tables["D"], this.DNumbers, this.DDateTimes);
            fixTable(this.Tables["H"], this.HNumbers, this.HDateTimes);
            log.Debug("Leaving fixFields");
        }

        private void fixTable(global::System.Data.DataTable dt, string[] numbers, string[] datetimes)
        {
            log.Debug("Entering fixTable: " + dt.TableName);
            foreach (global::System.Data.DataRow xr in dt.Rows)
            {
                foreach (global::System.Data.DataColumn dc in dt.Columns)
                {
                    if (!xr.IsNull(dc))
                    {
                        xr[dc] = ((string)xr[dc]).Replace("#", " ").Replace("$", " ").Trim();
                        if (numbers.Contains(dc.ColumnName))
                        {
                            Decimal dec = Decimal.Parse((string)xr[dc], this.inNfi);
                            xr[dc] = dec.ToString("N", this.outNfi);
                        }
                        if (datetimes.Contains(dc.ColumnName))
                        {
                            global::System.DateTime inDateTime;
                            try
                            {
                                inDateTime = global::System.DateTime.Parse((string)xr[dc], this.inDTfi);
                            }
                            catch (global::System.FormatException)
                            {
                                throw new global::System.SystemException("Wrong datetime format");
                            }
                            xr[dc] = inDateTime.ToString(this.DTOutFormat);

                        }
                    }
                }
            }
            log.Debug("Leaving fixTable: " + dt.TableName);
        }

        public string ToFormat(WebApiClient wac, string debugXML)
        {
            log.Debug("Entering ToFormat");
            DataSet s1ds = new DataSet();
            DataSet tmp = this.Copy();
            if (tmp.Tables["DS"] != null)
            {
                tmp.Tables.Remove("DS");
            }
            Type type = typeof(EIDataSet);
            Assembly ass = type.Assembly;
            s1ds.ReadXmlSchema(ass.GetManifestResourceStream(type.Namespace + "." + this.version + ".xsd"));
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    tmp.WriteXml(ms);
                    ms.Position = 0;
                    s1ds.ReadXml(ms, XmlReadMode.IgnoreSchema);
                }
            }
            catch (System.Xml.XmlException e)
            {
                throw new SystemException("Error in XML: " + e.Message);
            }
            if (log.IsDebugEnabled)
                s1ds.WriteXml(debugXML);
            string ret = wac.DatasetToFormat(s1ds);
            log.Debug(ret);
            log.Debug("Leaving ToFormat");
            return ret;
        }

        public string getVersion()
        {
            return this.version;
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        //-----------------------------------------------------------------------------
        // End My methods
        //
    }
}
