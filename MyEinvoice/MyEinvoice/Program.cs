﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace MyEinvoice
{
    class Program
    {
        static void Main(string[] args)
        {
            EInvoiceObject2 eio = null;
            if (args.Any())
            {
                try
                {
                    eio = new EInvoiceObject2(args[0]);
                    StreamReader sr = new StreamReader("invoice.txt");
                    eio.ParseInput(sr.BaseStream);
                    Microsoft.VisualBasic.Interaction.MsgBox(eio.SendIt());
                    //Console.ReadLine();
                }
                catch (SystemException e)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("there "+e.Message);
                    System.Environment.Exit(1);
                }
            }
            else
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Incorrect eINVOICE port setup");
                System.Environment.Exit(1);
            }
            System.Environment.Exit(0);
        }
    }
}
