﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.IO;
using System.Configuration;
using Microsoft.Win32;
using EInvoiceDataSet;
using EInvoiceApiClient;

namespace MyEinvoice
{
    class EInvoiceObject2
    {
        string eiSecret;
        string eiEncoding;
        bool grNumbers;
        bool grDates;
        Constants constants = new Constants();
        string eiUrl = string.Empty;
        string eiInfo = string.Empty;
        string eiStart = string.Empty;
        string eiEnd = string.Empty;
        string formatted = string.Empty;
        MyDataSet eids;

        public EInvoiceObject2(string printer)
        {
            RegistryKey eiPrintersKey = Registry.LocalMachine.OpenSubKey(constants.REGKEY);
            if (eiPrintersKey == null)
            {
                throw new SystemException("Non existent eINVOICE printers key in registry");
            }
            bool found = false;
            foreach (string pr in eiPrintersKey.GetSubKeyNames())
            {
                if (pr == printer)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                throw new SystemException(string.Format("Printer {0} not in registry", printer));
            }
            RegistryKey eiPrinterKey = eiPrintersKey.OpenSubKey(printer);
            this.eiSecret = (string)eiPrinterKey.GetValue(constants.RKSECRET, string.Empty);
            if (this.eiSecret == string.Empty)
            {
                throw new SystemException("No secret set");
            }
            this.eiEncoding = (string)eiPrinterKey.GetValue(constants.RKENCODING, "1253");
            this.grNumbers = (string)eiPrinterKey.GetValue(constants.RKNUMBERS, "1") == "1" ? true : false;
            this.grDates = (string)eiPrinterKey.GetValue(constants.RKDATES, "1") == "1" ? true : false;
            this.eiUrl = ConfigurationManager.AppSettings["EIUrl"];
            if (this.eiUrl == string.Empty || this.eiUrl == null)
            {
                this.eiUrl = @"https://einvoiceapp.softonecloud.com/";
            }
            this.eiInfo = ConfigurationManager.AppSettings["EIInfo"];
            if (this.eiInfo == string.Empty || this.eiInfo == null)
            {
                this.eiInfo = @"DREAM SOLUTIONS IKE";
            }
            this.eiStart = ConfigurationManager.AppSettings["EIStart"];
            if (this.eiStart == string.Empty || this.eiStart == null)
            {
                this.eiStart = @"<<";
            }
            this.eiEnd = ConfigurationManager.AppSettings["EIEnd"];
            if (this.eiEnd == string.Empty || this.eiEnd == null)
            {
                this.eiEnd = @">>";
            }
        }

        public void ParseInput(Stream inputStream)
        {
            List<byte> inputList = new List<byte>();
            Int32 inByte = 0;
            while (inByte != -1)
            {
                inByte = inputStream.ReadByte();
                if (inByte >= 32)
                {
                    inputList.Add((byte)inByte);
                }
            }

            byte[] bytes2 = { };
            if (this.eiEncoding == "1253")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(1253), Encoding.UTF8, inputList.ToArray());
            }
            else if (this.eiEncoding == "737")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(737), Encoding.UTF8, inputList.ToArray());
            }
            else if (this.eiEncoding != "UTF8")
            {
                throw new SystemException("Wrong encoding\r\nAccepted encodings: 1253, 737, UTF8");
            }
            string inputEInvoice = Encoding.UTF8.GetString(bytes2)
                                    .Replace(this.eiStart, "{{")
                                    .Replace(this.eiEnd, "}}")
                                    .Replace("&", "&amp;")
                                    .Replace("<", "&lt;")
                                    .Replace(">", "&gt;")
                                    .Replace("\"", "&quot;")
                                    .Replace("'", "&apos;")
                                    .Replace("{{", "<")
                                    .Replace("}}", ">");

            eids = new MyDataSet();

            try
            {
                using (Stream s = GenerateStreamFromString(inputEInvoice))
                {
                    eids.ReadXml(s);
                }
            }
            catch (System.Xml.XmlException e)
            {
                throw new SystemException("Problem with input\r\n" + e.Message);
            }
            try
            {
                eids.fixFields(this.grNumbers, this.grDates);
            }
            catch (SystemException e)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Here"+e.Message);
                System.Environment.Exit(1);
            }
            this.formatted = eids.ToFormat();
        }

        public string SendIt()
        {

            EInvoiceClient eic = new EInvoiceClient(eiUrl);
            EInvoiceClient.EIResponse response = eic.SendInvoice(this.eiSecret, constants.sendMethod, eiInfo, this.formatted);

            if (response.Status == EInvoiceClient.EIEnum.OK)
            {
                return "Επιτυχής καταχώρηση\r\nΑρ. Θεώρησης: " + response.Sign;
            }
            else if (response.Status == EInvoiceClient.EIEnum.WARNING)
            {
                return "Προειδοποίηση: " + response.Message + "\r\nΑρ. Θεώρησης: " + response.Sign;
            }
            else
            {
                return "Σφάλμα κατά την καταχώρηση:\r\n " + response.Message;
            }
        }

        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
