﻿namespace MyEinvoice
{
    public class Constants
    {
        public string REGKEY = @"Software\DreamSolutions\EInvoice\Printers";
        public string RKEINVOICE = @"Software\DreamSolutions\EInvoice";
        public string RKNAME = "Name";
        public string RKSECRET = "Secret";
        public string RKENCODING = "Encoding";
        public string RKNUMBERS = "GrNumbers";
        public string RKDATES = "GrDates";
        public string RKPORT = "Port";
        public string RKMONITORPORTS = @"System\CurrentControlSet\Control\Print\Monitors\Redirected Port\Ports";
        public string RKMONITOR = @"System\CurrentControlSet\Control\Print\Monitors\Redirected Port";
        public string RKINSTDIR = "InstallDir";
        public string sendMethod = "E";
    }
}
