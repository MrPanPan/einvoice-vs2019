﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Reflection;

namespace EInvoiceDataSet
{
    public class MyDataSet: DataSet
    {
        //---------------------------------------------------------------
        // My methods
        //---------------------------------------------------------------
        private string version;
        private string DTOutFormat;
        private string DOutFormat;
        private string TOutFormat;
        private string[] XNumbers;
        private string[] TNumbers;
        private string[] DNumbers;
        private string[] HNumbers;
        private string[] DDates;
        private string[] HDates;
        private string[] HDateTimes;
        private string[] HTimes;
        private NumberFormatInfo inNfi;
        private NumberFormatInfo outNfi;
        private DateTimeFormatInfo inDTfi;


        public MyDataSet()
        {
            Type type = typeof(MyDataSet);
            Assembly ass = type.Assembly;
            this.ReadXmlSchema(ass.GetManifestResourceStream(type.Namespace + ".schema.xsd"));
            this.XNumbers = Properties.Settings.Default.XNumbers.Cast<string>().ToArray();
            this.TNumbers = Properties.Settings.Default.TNumbers.Cast<string>().ToArray();
            this.DNumbers = Properties.Settings.Default.DNumbers.Cast<string>().ToArray();
            this.HNumbers = Properties.Settings.Default.HNumbers.Cast<string>().ToArray();
            this.DDates = Properties.Settings.Default.DDates.Cast<string>().ToArray();
            this.HDates = Properties.Settings.Default.HDates.Cast<string>().ToArray();
            this.HDateTimes = Properties.Settings.Default.HDateTimes.Cast<string>().ToArray();
            this.HTimes = Properties.Settings.Default.HTimes.Cast<string>().ToArray();
            this.DTOutFormat = Properties.Settings.Default.dtOutFormat;
            this.DOutFormat = Properties.Settings.Default.dOutFormat;
            this.TOutFormat = Properties.Settings.Default.tOutFormat;
            this.version = Properties.Settings.Default.version;
        }

        public void fixFields(bool grNumbers, bool grDates)
        {
            string[] empty = { };

            CultureInfo grCulture = new CultureInfo("el-GR");
            CultureInfo usCulture = new CultureInfo("en-US");
            if (grNumbers)
            {
                this.inNfi = grCulture.NumberFormat;
            }
            else
            {
                this.inNfi = usCulture.NumberFormat;
            }
            this.outNfi = usCulture.NumberFormat;
            outNfi.NumberGroupSeparator = "";
            if (grDates)
            {
                this.inDTfi = grCulture.DateTimeFormat;
            }
            else
            {
                this.inDTfi = usCulture.DateTimeFormat;
            }
            fixTable(this.Tables["X"], this.XNumbers, empty, empty, empty);
            fixTable(this.Tables["T"], this.TNumbers, empty, empty, empty);
            fixTable(this.Tables["A"], empty, empty, empty, empty);
            fixTable(this.Tables["D"], this.DNumbers, empty, this.DDates, empty);
            fixTable(this.Tables["H"], this.HNumbers, this.HDateTimes, this.HDates, this.HTimes);
        }

        private void fixTable(global::System.Data.DataTable dt, string[] numbers, string[] datetimes, string[] dates, string[] times)
        {
            foreach (global::System.Data.DataRow xr in dt.Rows)
            {
                foreach (global::System.Data.DataColumn dc in dt.Columns)
                {
                    if (!xr.IsNull(dc))
                    {
                        xr[dc] = ((string)xr[dc]).Replace("#", " ").Replace("$", " ").Trim();
                        if (numbers.Contains(dc.ColumnName))
                        {
                            Decimal dec = Decimal.Parse((string)xr[dc], this.inNfi);
                            xr[dc] = dec.ToString("N", this.outNfi);
                        }
                        if (datetimes.Contains(dc.ColumnName))
                        {
                            global::System.DateTime inDateTime;
                            try
                            {
                                inDateTime = global::System.DateTime.Parse((string)xr[dc], this.inDTfi);
                            }
                            catch (global::System.FormatException)
                            {
                                throw new global::System.SystemException("Wrong datetime format");
                            }
                            xr[dc] = inDateTime.ToString(this.DTOutFormat);

                        }
                        if (dates.Contains(dc.ColumnName))
                        {
                            global::System.DateTime inDateTime;
                            try
                            {
                                inDateTime = global::System.DateTime.Parse((string)xr[dc], this.inDTfi);
                            }
                            catch (global::System.FormatException)
                            {
                                throw new global::System.SystemException("Wrong date format");
                            }
                            xr[dc] = inDateTime.ToString(this.DOutFormat);

                        }
                        if (times.Contains(dc.ColumnName))
                        {
                            global::System.DateTime inDateTime;
                            try
                            {
                                inDateTime = global::System.DateTime.Parse((string)xr[dc], this.inDTfi);
                            }
                            catch (global::System.FormatException)
                            {
                                throw new global::System.SystemException("Wrong time format");
                            }
                            xr[dc] = inDateTime.ToString(this.TOutFormat);

                        }
                    }
                }
            }
        }

        public string ToFormat()
        {
            StringBuilder sb = new StringBuilder();
            foreach (DataTable dt in this.Tables)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sb.Append(dt.TableName + "$");
                        foreach (DataColumn dc in dt.Columns)
                        {
                            sb.Append(dr[dc.ColumnName] + "#");
                        }
                        sb.Remove(sb.Length - 1, 1);
                    }
                }
            }
            return sb.ToString();
        }

        public string getVersion()
        {
            return this.version;
        }
        //-----------------------------------------------------------------------------
        // End My methods
        //
    }
}
