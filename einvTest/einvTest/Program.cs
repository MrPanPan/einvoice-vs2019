﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DreamSolutions.EIDataSet;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.IO;
using System.Configuration;
using eINVOICEWebApiClient;

namespace DreamSolutions.einvTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string eiSecret = string.Empty;
            if (args.Any())
            {
                eiSecret = args[0];
            }
            else
            {
                Microsoft.VisualBasic.Interaction.MsgBox("No secret given");
                return;
            }

            Stream inputStream = Console.OpenStandardInput();
            List<byte> inputList = new List<byte>();
            Int32 inByte = 0;
            while (inByte != -1)
            {
                inByte = inputStream.ReadByte();
                if (inByte >= 32)
                {
                    inputList.Add((byte)inByte);
                }
            }

            string encoding = ConfigurationManager.AppSettings["EIEncoding"];
            byte[] bytes2 = { };
            if (encoding == "1253")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(1253), Encoding.UTF8, inputList.ToArray());
            }
            else if (encoding == "737")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(737), Encoding.UTF8, inputList.ToArray());
            }
            else if (encoding != "UTF8")
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Wrong encoding\r\nAccepted encodings: 1253, 737, UTF8");
                return;
            }
            string inputEInvoice = Encoding.UTF8.GetString(bytes2).Replace("<<", "<").Replace(">>", ">").Replace("&", "&amp;");


            EInvoiceDataSet eids = new EInvoiceDataSet();
            try
            {
                using (Stream s = GenerateStreamFromString(inputEInvoice))
                {
                    eids.ReadXml(s, XmlReadMode.IgnoreSchema);
                }
            }
            catch (System.Xml.XmlException)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Problem with input");
                return;
            }
            string dtOutFormat = "yyyyMMdd HHmmss";
            string dOutFormat = "yyyyMMdd";
            string tOutFormat = "HHmmss";
            string WebApiVersion = "GR20140225H0310D080X03T02";
            string sendMethod = "E";
            bool grNumbers = ConfigurationManager.AppSettings["GRNumbers"] == "1" ? true : false;
            bool grDates = ConfigurationManager.AppSettings["GRDates"] == "1" ? true : false;
            string eiUrl = ConfigurationManager.AppSettings["EIUrl"];
            //string eiSecret = ConfigurationManager.AppSettings["EISecret"];
            string eiInfo = ConfigurationManager.AppSettings["EIInfo"];
            try
            {
                eids.fixFields(dtOutFormat, dOutFormat, tOutFormat, grNumbers, grDates);
            }
            catch (SystemException e)
            {
                Microsoft.VisualBasic.Interaction.MsgBox(e.Message);
                return;
            }
            string res = FormatterHelper.Formater.DatasetToFormat(eids, WebApiVersion);

            WebApiClient wac = new WebApiClient(eiUrl, WebApiVersion);
            WebApiClient.SendDocumentResponse response = wac.SendDocument(eiSecret, sendMethod, eiInfo, res);
            if (response.Status == WebApiClient.StatusEnum.OK)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Επιτυχής καταχώρηση\r\nΑρ. Θεώρησης: " + response.Sign);
            }
            else if (response.Status == WebApiClient.StatusEnum.WARNING)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Προειδοποίηση: " + response.Message + "\r\nΑρ. Θεώρησης: " + response.Sign);
            }
            else
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Σφάλμα κατά την καταχώρηση:\r\n " + response.Message);
            }
        }
        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
