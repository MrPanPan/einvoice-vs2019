﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;


namespace printermanage
{
    class Program
    {
        static void Main(string[] args)
        {
            ChangeCodePage(args[0], args[1]);
            Console.ReadLine();

        }

        public static void ChangeCodePage(string printerName, string codepage)
        {
            if (!(codepage == "737" || codepage == "1253")) return;
            Version win8Version = new Version(WIN8VERSION);
            Version osVersion = Environment.OSVersion.Version;
            if (osVersion.CompareTo(win8Version) == -1)
            {
                IntPtr pHandle;
                PRINTER_DEFAULTS defaults = new PRINTER_DEFAULTS();
                defaults.DesiredAccess = ACCESS_MASK.PRINTER_ACCESS_ADMINISTER;
                OpenPrinter(printerName, out pHandle, defaults);
                if (pHandle.ToString() == "0") return;
                string pValueName = "TTY DeviceConfig";
                uint nSize = 1;
                uint needed = 0;
                uint type = 0;
                byte[] tmp = new byte[nSize];
                GetPrinterData(pHandle, pValueName, out type, tmp, nSize, out needed);
                nSize = needed;
                byte[] pData = new byte[nSize];
                GetPrinterData(pHandle, pValueName, out type, pData, nSize, out needed);
                if (codepage == "737") set737(ref pData);
                else set1253(ref pData);
                SetPrinterData(pHandle, pValueName, REGISTRY_TYPES.REG_BINARY, pData, nSize);
                ClosePrinter(pHandle);
            }
        }

        public const string WIN8VERSION = "6.2.0.0";

        static void set737(ref byte[] pd)
        {
            pd[4] = 0xff;
            pd[5] = 0xff;
            pd[6] = 0xff;
            pd[7] = 0xff;
            pd[8] = 0x01;
        }

        static void set1253(ref byte[] pd)
        {
            pd[4] = 0xe5;
            pd[5] = 0x04;
            pd[6] = 0x00;
            pd[7] = 0x00;
            pd[8] = 0x00;
        }
        [DllImport("winspool.drv", CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern unsafe bool OpenPrinter(string pPrinterName, out IntPtr phPrinter, PRINTER_DEFAULTS pDefault);

        [DllImport("winspool.drv", CharSet = CharSet.Auto)]
        static extern uint GetPrinterData(IntPtr hPrinter, string pValueName, out uint pType, byte[] pData, uint nSize, out uint pcbNeeded);

        [DllImport("winspool.drv", CharSet = CharSet.Auto)]
        static extern uint SetPrinterData(IntPtr hPrinter, string pValueName, uint Type, byte[] pData, uint cbData);

        [DllImport("winspool.drv")]
        static extern int ClosePrinter(IntPtr hPrinter);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal class PRINTER_DEFAULTS
        {
            /// <summary>
            /// Pointer to a null-terminated string that specifies the 
            /// default data type for a printer.
            /// </summary>
            public IntPtr pDatatype;

            /// <summary>
            /// Pointer to a DEVMODE structure that identifies the 
            /// default environment and initialization data for a printer.
            /// </summary>
            public IntPtr pDevMode;

            /// <summary>
            /// Specifies desired access rights for a printer. 
            /// The <see cref="OpenPrinter(string, out IntPtr, IntPtr)"/> function uses
            /// this member to set access rights to the printer. These rights can affect 
            /// the operation of the SetPrinter and DeletePrinter functions.
            /// </summary>
            public ACCESS_MASK DesiredAccess;
        }

        [Flags]
        public enum ACCESS_MASK : uint
        {
            DELETE = 0x00010000,
            READ_CONTROL = 0x00020000,
            WRITE_DAC = 0x00040000,
            WRITE_OWNER = 0x00080000,
            SYNCHRONIZE = 0x00100000,

            STANDARD_RIGHTS_REQUIRED = 0x000F0000,

            STANDARD_RIGHTS_READ = 0x00020000,
            STANDARD_RIGHTS_WRITE = 0x00020000,
            STANDARD_RIGHTS_EXECUTE = 0x00020000,

            STANDARD_RIGHTS_ALL = 0x001F0000,

            SPECIFIC_RIGHTS_ALL = 0x0000FFFF,

            ACCESS_SYSTEM_SECURITY = 0x01000000,

            MAXIMUM_ALLOWED = 0x02000000,

            GENERIC_READ = 0x80000000,
            GENERIC_WRITE = 0x40000000,
            GENERIC_EXECUTE = 0x20000000,
            GENERIC_ALL = 0x10000000,

            DESKTOP_READOBJECTS = 0x00000001,
            DESKTOP_CREATEWINDOW = 0x00000002,
            DESKTOP_CREATEMENU = 0x00000004,
            DESKTOP_HOOKCONTROL = 0x00000008,
            DESKTOP_JOURNALRECORD = 0x00000010,
            DESKTOP_JOURNALPLAYBACK = 0x00000020,
            DESKTOP_ENUMERATE = 0x00000040,
            DESKTOP_WRITEOBJECTS = 0x00000080,
            DESKTOP_SWITCHDESKTOP = 0x00000100,

            WINSTA_ENUMDESKTOPS = 0x00000001,
            WINSTA_READATTRIBUTES = 0x00000002,
            WINSTA_ACCESSCLIPBOARD = 0x00000004,
            WINSTA_CREATEDESKTOP = 0x00000008,
            WINSTA_WRITEATTRIBUTES = 0x00000010,
            WINSTA_ACCESSGLOBALATOMS = 0x00000020,
            WINSTA_EXITWINDOWS = 0x00000040,
            WINSTA_ENUMERATE = 0x00000100,
            WINSTA_READSCREEN = 0x00000200,

            WINSTA_ALL_ACCESS = 0x0000037F,

            PRINTER_ACCESS_ADMINISTER = 0x00000004
        }

        public static class REGISTRY_TYPES {
            public static uint REG_NONE = 0;
            public static uint REG_SZ = 1;
            public static uint REG_EXPAND_SZ = 2;
            public static uint REG_BINARY = 3;
            public static uint REG_DWORD = 4;
            public static uint REG_DWORD_LITTLE_ENDIAN = 4;
            public static uint REG_DWORD_BIG_ENDIAN = 5;
            public static uint REG_LINK = 6;
            public static uint REG_MULTI_SZ = 7;
            public static uint REG_RESOURCE_LIST = 8;
            public static uint REG_FULL_RESOURCE_DESCRIPTOR = 9;
            public static uint REG_RESOURCE_REQUIREMENTS_LIST = 10;
            public static uint REG_QWORD = 11;
            public static uint REG_QWORD_LITTLE_ENDIAN = 11;
        }
    }
}
