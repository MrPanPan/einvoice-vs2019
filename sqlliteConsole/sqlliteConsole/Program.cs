﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;
using System.Data;


namespace sqlliteConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLiteConnection sql_con = new SQLiteConnection("Data Source=logdb.sqlite;Version=3;");
            SQLiteCommand cmd;
            sql_con.Open();
            using (SQLiteTransaction sqlTransaction = sql_con.BeginTransaction())
            {
                cmd = new SQLiteCommand();
                cmd = sql_con.CreateCommand();
                cmd.CommandText = "create table if not exists log (invID TEXT, dateSend DATETIME, invDate DATETIME, invSeries TEXT, invNumber TEXT, status INTEGER, sign TEXT)";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cmd = sql_con.CreateCommand();
                cmd.CommandText = "create index if not exists invID_idx on log(invID)";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cmd = sql_con.CreateCommand();
                cmd.CommandText = "create index if not exists dateSend_idx on log(dateSend DESC)";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cmd = sql_con.CreateCommand();
                cmd.CommandText = "create index if not exists invDate_idx on log(invDate DESC)";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cmd = sql_con.CreateCommand();
                cmd.CommandText = "create index if not exists invNumber_idx on log(invNumber)";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cmd = sql_con.CreateCommand();
                cmd.CommandText = "INSERT INTO log (invID, dateSend, invDate, invSeries, invNumber, status, sign) values(@invID, @dateSend, @invDate, @invSeries, @invNumber, @status, @sign)";
                cmd.Parameters.AddWithValue("@invID", "4");
                cmd.Parameters.AddWithValue("@dateSend", DateTime.Now.ToString("s"));
                cmd.Parameters.AddWithValue("@invDate", DateTime.Now.ToString("s"));
                cmd.Parameters.AddWithValue("@invSeries", "TDA");
                cmd.Parameters.AddWithValue("@invNumber", "454645");
                cmd.Parameters.AddWithValue("@status", 1);
                cmd.Parameters.AddWithValue("@sign", "321321321");
                cmd.ExecuteNonQuery();
                sqlTransaction.Commit();
            }

            cmd.Dispose();
            cmd = new SQLiteCommand("select * from log", sql_con);
            SQLiteDataAdapter da = new SQLiteDataAdapter();
            DataTable dt = new DataTable();
            da.SelectCommand = cmd;
            da.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                Console.WriteLine(row[0] +" "+row[1]+" "+row[2]+" "+row[3]);
            }
            cmd.Dispose();
            sql_con.Close();
            Console.ReadLine();
        }
    }
}
