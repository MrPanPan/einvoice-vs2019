#using "EInvoiceDLL.dll"
#include "stdafx.h"
#include <msclr\auto_gcroot.h>
 
using namespace System::Runtime::InteropServices; // Marshal
 
class EInvoiceDLLWrapperPrivate
{
	public: msclr::auto_gcroot<DreamSolutions::EInvoiceDLL::EInvoice^> einvoice;
};
 
class __declspec(dllexport) EInvoiceDLLWrapper
{
    private: EInvoiceDLLWrapperPrivate* _private;
 
    public: EInvoiceDLLWrapper(const char* inputtext, const char* secret)
    {
        _private = new EInvoiceDLLWrapperPrivate();
        _private->einvoice = gcnew DreamSolutions::EInvoiceDLL::EInvoice(gcnew System::String(inputtext), gcnew System::String(secret));
    }
 
    public: int SendIt()
    {
        return _private->einvoice->SendIt();
    }
 
    public: ~EInvoiceDLLWrapper()
    {
        delete _private;
    }
};
