﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Configuration;
using eINVOICEWebApiClient;

namespace DreamSolutions.EInvoiceDLL
{
    public class EInvoice
    {
        private string inputText;
        private string eiSecret;
        public EInvoice(string inputtext, string secret)
        {
            this.inputText = inputtext;
            this.eiSecret = secret;
        }
        public int SendIt()
        {
            Configuration oConfig = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);
            List<byte> inputList = new List<byte>();
            foreach (byte b in this.inputText)
            {
                inputList.Add(b);
            }
            string encoding = oConfig.AppSettings.Settings["EIEncoding"].Value;
            byte[] bytes2 = { };
            if (encoding == "1253")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(1253), Encoding.UTF8, inputList.ToArray());
            }
            else if (encoding == "737")
            {
                bytes2 = Encoding.Convert(Encoding.GetEncoding(737), Encoding.UTF8, inputList.ToArray());
            }
            else if (encoding != "UTF8")
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Wrong encoding\r\nAccepted encodings: 1253, 737, UTF8", Microsoft.VisualBasic.MsgBoxStyle.OkOnly, "EInvoice");
                return 1;
            }
            string inputEInvoice = Encoding.UTF8.GetString(bytes2).Replace("<<", "<").Replace(">>", ">").Replace("&", "&amp;");
            EInvoiceDataSet eids = new EInvoiceDataSet();
            try
            {
                using (Stream s = GenerateStreamFromString(inputEInvoice))
                {
                    eids.ReadXml(s, XmlReadMode.IgnoreSchema);
                }
            }
            catch (System.Xml.XmlException)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Problem with input", Microsoft.VisualBasic.MsgBoxStyle.OkOnly, "EInvoice");
                return 1;
            }
            string dtOutFormat = "yyyyMMdd HHmmss";
            string dOutFormat = "yyyyMMdd";
            string tOutFormat = "HHmmss";
            string WebApiVersion = "GR20140225H0310D080X03T02";
            string sendMethod = "E";
            bool grNumbers = oConfig.AppSettings.Settings["GRNumbers"].Value == "1" ? true : false;
            bool grDates = oConfig.AppSettings.Settings["GRDates"].Value == "1" ? true : false;
            string eiUrl = oConfig.AppSettings.Settings["EIUrl"].Value;
            //string eiSecret = ConfigurationManager.AppSettings["EISecret"];
            string eiInfo = oConfig.AppSettings.Settings["EIInfo"].Value;
            try
            {
                eids.fixFields(dtOutFormat, dOutFormat, tOutFormat, grNumbers, grDates);
            }
            catch (SystemException e)
            {
                Microsoft.VisualBasic.Interaction.MsgBox(e.Message, Microsoft.VisualBasic.MsgBoxStyle.OkOnly, "EInvoice");
                return 1;
            }
            string res = FormatterHelper.Formater.DatasetToFormat(eids, WebApiVersion);

            WebApiClient wac = new WebApiClient(eiUrl, WebApiVersion);
            WebApiClient.SendDocumentResponse response = wac.SendDocument(this.eiSecret, sendMethod, eiInfo, res);
            if (response.Status == WebApiClient.StatusEnum.OK)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Επιτυχής καταχώρηση\r\nΑρ. Θεώρησης: " + response.Sign, Microsoft.VisualBasic.MsgBoxStyle.OkOnly, "EInvoice");
                return 0;
            }
            else if (response.Status == WebApiClient.StatusEnum.WARNING)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Προειδοποίηση: " + response.Message + "\r\nΑρ. Θεώρησης: " + response.Sign, Microsoft.VisualBasic.MsgBoxStyle.OkOnly, "EInvoice");
                return 1;
            }
            else
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Σφάλμα κατά την καταχώρηση:\r\n " + response.Message, Microsoft.VisualBasic.MsgBoxStyle.OkOnly, "EInvoice");
                return 1;
            }
        }
        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
