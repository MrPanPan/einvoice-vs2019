class EInvoiceDLLWrapperPrivate;
class __declspec(dllexport) EInvoiceDLLWrapper
{
    private: EInvoiceDLLWrapperPrivate* _private;
    public: EInvoiceDLLWrapper(const char* inputtext, const char* secret);
    public: ~EInvoiceDLLWrapper();
    public: int SendIt();
};
