﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace xsdcreation
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet ds = new DataSet("EInvoiceDataSet");
            DataTable h = ds.Tables.Add("H");
            for (int i = 1; i <= 311; i++)
            {
                h.Columns.Add("f"+i.ToString(), typeof(string));
            }
            DataTable d = ds.Tables.Add("D");
            for (int i = 1; i <= 81; i++)
            {
                d.Columns.Add("f"+i.ToString(), typeof(string));
            }
            DataTable a = ds.Tables.Add("A");
            for (int i = 1; i <= 5; i++)
            {
                a.Columns.Add("f" + i.ToString(), typeof(string));
            }
            DataTable t = ds.Tables.Add("T");
            for (int i = 1; i <= 3; i++)
            {
                t.Columns.Add("f" + i.ToString(), typeof(string));
            }
            DataTable x = ds.Tables.Add("X");
            for (int i = 1; i <= 4; i++)
            {
                x.Columns.Add("f" + i.ToString(), typeof(string));
            }
            ds.WriteXmlSchema(@"c:\Users\Supervisor\develop\datasettests\EInvoiceDataSet.xsd");
        }
    }
}
