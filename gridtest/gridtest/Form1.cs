﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReadWriteCsv;
using System.IO;
using System.Diagnostics;

namespace gridtest
{
    public partial class Form1 : Form
    {
        private DataSet1 ds = new DataSet1();
        private BindingSource bs = new BindingSource();
        private DataGridView dgv = new DataGridView();
        private DataGridViewTextBoxColumn invIDDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn dateSendDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn invDateDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn invSeriesDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn invNumberDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn = new DataGridViewTextBoxColumn();
        private DataGridViewLinkColumn signDataGridViewTextBoxColumn = new DataGridViewLinkColumn();

        public Form1()
        {
            InitializeComponent();

            // GridView
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AutoGenerateColumns = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.invIDDataGridViewTextBoxColumn,
            this.dateSendDataGridViewTextBoxColumn,
            this.invDateDataGridViewTextBoxColumn,
            this.invSeriesDataGridViewTextBoxColumn,
            this.invNumberDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.signDataGridViewTextBoxColumn});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgv.Location = new System.Drawing.Point(0, 150);
            this.dgv.Name = "dgv";
            this.dgv.DataSource = bs;
            this.dgv.ReadOnly = true;
            this.dgv.Size = new System.Drawing.Size(877, 380);
            this.dgv.TabIndex = 0;
            this.dgv.DataBindingComplete += dataGridView1_DataBindingComplete;

            // Dataset
            this.ds.DataSetName = "DataSet1";
            this.ds.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;

            // BindingSource
            //this.bs.DataMember = "LogTable";
            this.bs.DataSource = this.ds.Tables["LogTable"].DefaultView;
            //this.bs.Filter = "dateSend >= '2015-09-27T15:30:00'";
            // 
            // invIDDataGridViewTextBoxColumn
            // 
            this.invIDDataGridViewTextBoxColumn.DataPropertyName = "invID";
            this.invIDDataGridViewTextBoxColumn.HeaderText = "invID";
            this.invIDDataGridViewTextBoxColumn.Name = "invIDDataGridViewTextBoxColumn";
            this.invIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.invIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // dateSendDataGridViewTextBoxColumn
            // 
            this.dateSendDataGridViewTextBoxColumn.DataPropertyName = "dateSend";
            this.dateSendDataGridViewTextBoxColumn.HeaderText = "Ημ. Καταχώρησης";
            this.dateSendDataGridViewTextBoxColumn.Name = "dateSendDataGridViewTextBoxColumn";
            this.dateSendDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // invDateDataGridViewTextBoxColumn
            // 
            this.invDateDataGridViewTextBoxColumn.DataPropertyName = "invDate";
            this.invDateDataGridViewTextBoxColumn.HeaderText = "Ημ. Παραστατικού";
            this.invDateDataGridViewTextBoxColumn.Name = "invDateDataGridViewTextBoxColumn";
            this.invDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // invSeriesDataGridViewTextBoxColumn
            // 
            this.invSeriesDataGridViewTextBoxColumn.DataPropertyName = "invSeries";
            this.invSeriesDataGridViewTextBoxColumn.HeaderText = "Σειρά";
            this.invSeriesDataGridViewTextBoxColumn.Name = "invSeriesDataGridViewTextBoxColumn";
            this.invSeriesDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // invNumberDataGridViewTextBoxColumn
            // 
            this.invNumberDataGridViewTextBoxColumn.DataPropertyName = "invNumber";
            this.invNumberDataGridViewTextBoxColumn.HeaderText = "Αρ. παραστατικού";
            this.invNumberDataGridViewTextBoxColumn.Name = "invNumberDataGridViewTextBoxColumn";
            this.invNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.invNumberDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // statusDataGridViewTextBoxColumn
            // 
            this.statusDataGridViewTextBoxColumn.DataPropertyName = "status";
            this.statusDataGridViewTextBoxColumn.HeaderText = "status";
            this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
            this.statusDataGridViewTextBoxColumn.ReadOnly = true;
            this.statusDataGridViewTextBoxColumn.Visible = false;
            // 
            // signDataGridViewTextBoxColumn
            // 
            this.signDataGridViewTextBoxColumn.DataPropertyName = "sign";
            this.signDataGridViewTextBoxColumn.HeaderText = "Υπογραφή";
            this.signDataGridViewTextBoxColumn.Name = "signDataGridViewTextBoxColumn";
            this.signDataGridViewTextBoxColumn.ReadOnly = true;
            this.signDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.signDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;

            this.Controls.Add(dgv);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable dt = this.ds.Tables["LogTable"];
            DataRow row;
            using (CsvFileReader reader = new CsvFileReader("log.csv"))
            {
                CsvRow csvrow = new CsvRow();
                while (reader.ReadRow(csvrow))
                {
                    row = dt.NewRow();
                    row["invID"] = csvrow[0];
                    row["dateSend"] = DateTime.Parse(csvrow[1]);
                    row["invDate"] = DateTime.Parse(csvrow[2]);
                    row["invSeries"] = csvrow[3];
                    row["invNumber"] = csvrow[4];
                    row["status"] = csvrow[5];
                    row["sign"] = csvrow[6];
                    dt.Rows.Add(row);
                }
            }
            dt.AcceptChanges();
        }

        void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            this.dgv.Sort(this.dgv.Columns[1], ListSortDirection.Descending);
            this.dgv.Columns[1].HeaderCell.SortGlyphDirection = SortOrder.Descending;
        }

        private void inumber_TextChanged(object sender, EventArgs e)
        {
            var i = ((TextBox)sender).Text;
            this.bs.Filter = String.Format("invNumber LIKE '{0}'", i);
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            var i = ((DateTimePicker)sender).Value;
            Debug.WriteLine(i);
            this.bs.Filter = String.Format("dateSend >= '{0:yyyy-MM-ddTHH:mm:ss}'", i);
        }
    }
}
