﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Data.Common;

namespace DreamSolutions.ERPConnectors
{
    public class AtlantisConnector
    {
        private string _id;
        private string _sign;
        private string _printerName;
        private string _workingDir;
        public AtlantisConnector(string id, string sign, string printerName, string workingDir)
        {
            this._id = id;
            this._sign = sign;
            this._printerName = printerName;
            this._workingDir = workingDir;
        }
        public bool DoUpdate()
        {
            List<Config> configs;
            try
            {
                configs = JsonConvert.DeserializeObject<List<Config>>(File.ReadAllText(this._workingDir + @"\AtlantisConnector.json"));
            }
            catch (Newtonsoft.Json.JsonException ex)
            {
                throw new SystemException(ex.Message);
            }

            foreach (Config c in configs)
            {
                if (c.printers.Contains(this._printerName))
                {
                    DbProviderFactory factory = DbProviderFactories.GetFactory(c.provider);
                    DbConnection sql_con = factory.CreateConnection();
                    sql_con.ConnectionString = c.connString;
                    var cmd = sql_con.CreateCommand();
                    int affected = 0;
                    try
                    {
                        sql_con.Open();
                        using (var sqlTransaction = sql_con.BeginTransaction())
                        {
                            cmd.CommandText = string.Format("UPDATE {1} SET {2}='{4}' where {3}={0}",
                                    this._id,
                                    c.tableName,
                                    c.fieldSignName,
                                    c.fieldIdName,
                                    this._sign
                                    );
                            cmd.Transaction = sqlTransaction;
                            affected = cmd.ExecuteNonQuery();
                            sqlTransaction.Commit();
                        }
                    }
                    finally
                    {
                        cmd.Dispose();
                        sql_con.Close();
                    }

                    return affected == 1;
                }
            }
            throw new NotImplementedException();
        }

        private class Config
        {
            public List<string> printers { get; set; }
            public string provider { get; set; }
            public string connString { get; set; }
            public string tableName { get; set; }
            public string fieldIdName { get; set; }
            public string fieldSignName { get; set; }
        }
    }
}
